/*
Copyright (c) 2012, Broadcom Europe Ltd
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the copyright holder nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* This file is adapted from the hello_triangle example found in 
   /opt/vc/src/hello_pi/hello_triangle/triangle.c
   as it is used without major modification the above notice is included */

#include "pop_client_display.h"
#include "math.h"
#include "bcm_host.h"

/* creates and initializes a graphics context and surface */
void
init_ogl(SCREEN_T *screen)
{
   int32_t success = 0;
   EGLBoolean result;
   EGLint num_config;

   static EGL_DISPMANX_WINDOW_T nativewindow;
   
   DISPMANX_ELEMENT_HANDLE_T dispman_element;
   DISPMANX_DISPLAY_HANDLE_T dispman_display;
   DISPMANX_UPDATE_HANDLE_T dispman_update;

   VC_RECT_T dst_rect;
   VC_RECT_T src_rect;

   /* We request a display with only 4 bpp
      colour values */
   static EGLint attributes[] =
   {
      EGL_RED_SIZE, 4,
      EGL_GREEN_SIZE, 4,
      EGL_BLUE_SIZE, 4,
      EGL_ALPHA_SIZE, 4,
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_NONE
   };
   
   EGLConfig config;

   // get a display connection
   screen->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
   assert(screen->display != EGL_NO_DISPLAY);

   // initialize the display connection
   result = eglInitialize(screen->display, NULL, NULL);

   if (result == EGL_FALSE) {
      fprintf(stderr, "couldn't initialize display\n");
      bcm_host_deinit();
      exit(EXIT_FAILURE);
   }


   // get appropriate frame buffer configuration
   result = eglChooseConfig(screen->display, attributes, 
                           &config, 1, &num_config);

   if (result == EGL_FALSE) {
      fprintf(stderr, "unable to get suitable display configuration\n");
      bcm_host_deinit();
      exit(EXIT_FAILURE);
   }


   // create rendering context
   screen->context = eglCreateContext(screen->display, 
                                      config, EGL_NO_CONTEXT, NULL);

   // create surface
   success = graphics_get_display_size(0 /* LCD */, 
                                       &screen->width,
                                       &screen->height);

   if (success < 0) {
      fprintf(stderr, "unable to read display size\n");
      bcm_host_deinit();
      exit(EXIT_FAILURE);
   }


   /* set up source and destination rectangles */
   dst_rect.x = 0;
   dst_rect.y = 0;
   dst_rect.width = screen->width;
   dst_rect.height = screen->height;
   printf("screen width = %d screen height = %d\n", screen->width, screen->height);

   src_rect.x = 0;
   src_rect.y = 0;
   src_rect.width = screen->width << 16;
   src_rect.height = screen->height << 16;

   /* open and start display 0 */
   dispman_display = vc_dispmanx_display_open(0);
   dispman_update = vc_dispmanx_update_start(0);

   dispman_element = vc_dispmanx_element_add(dispman_update, dispman_display,
                                             0, &dst_rect, 0,
                                             &src_rect,
                                             DISPMANX_PROTECTION_NONE,
                                             0, 0, 0);

   nativewindow.element = dispman_element;
   nativewindow.width = screen->width;
   nativewindow.height = screen->height;
   vc_dispmanx_update_submit_sync (dispman_update);
   
   /* create the final surface */
   screen->surface = eglCreateWindowSurface(screen->display, 
                                            config,
                                            &nativewindow,
                                            NULL);

   /* make it our active surface */
   result = eglMakeCurrent(screen->display, screen->surface, screen->surface,
                           screen->context);

   if (result == EGL_FALSE) {
      fprintf(stderr, "couldn't make surface active\n");
      bcm_host_deinit();
      exit(EXIT_FAILURE);
   }


   /* set a default background color */
   glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
}

/* set up our frustrum for projection & view culling */
void 
init_projection(SCREEN_T *screen)
{
   float near = 1.0f;
   float far = 10.0f;
   float height;
   float width = 1.0;

   height = near * (float)tan(45.0f / 2.0f / 100.0f * M_PI);
   glFrustumf(-width, width, -height, height, near, far);
  
   /* enable alpha effects */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   
   /* enable vertex arrays */
   glEnableClientState(GL_VERTEX_ARRAY);
   glEnableClientState(GL_COLOR_ARRAY);
   glEnableClientState(GL_NORMAL_ARRAY);
}


