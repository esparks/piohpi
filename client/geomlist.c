#include "geomlist.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

void GListInit(g_list* glist)
{
  assert(glist != NULL);
  glist->count = 0;
  glist->head = NULL;
}

int GListInsert(g_list* glist, g_data* gdata)
{
  t_node* new = NULL;
  t_node* curr = NULL;
  t_node* prev = NULL;
  
  assert(glist != NULL);
  
  new = malloc(sizeof(t_node));
  new->data = gdata;
  new->next = NULL;
  
  assert(new != NULL);
  
  curr = glist->head;
  
  if (curr == NULL) {
    glist->count++;
    glist->head = new;
    return EXIT_SUCCESS;
  }
  
  while (curr != NULL) {
    prev = curr;
    curr = curr->next;
  }
  
  glist->count++;
  prev->next = new;
  
  return EXIT_SUCCESS;
  
}

void GListFree(g_list* glist)
{
  t_node* next;
  t_node* curr;
  assert(glist != NULL);
  
  curr = glist->head;
  while (curr != NULL) {
    next = curr->next;
    free(curr->data->indices);
    // delete any buffers
    glDeleteBuffers(1, &curr->data->indexBuffer);
    free(curr->data);
    free(curr);
    curr = next;
  }
  
  free(glist);
}




/* GLuint List */
void GLIListInit(gli_list* glist)
{
  assert(glist != NULL);
  glist->count = 0;
  glist->head = NULL;
}

int GLIListInsert(gli_list* glist, GLuint val)
{
  gli_node* new = NULL;
  gli_node* curr = NULL;
  gli_node* prev = NULL;
  
  assert(glist != NULL);
  
  new = malloc(sizeof(gli_node));
  new->value = val;
  new->next = NULL;
  
  assert(new != NULL);
  
  curr = glist->head;
  
  if (curr == NULL) {
    glist->count++;
    glist->head = new;
    return EXIT_SUCCESS;
  }
  
  while (curr != NULL) {
    prev = curr;
    curr = curr->next;
  }
  
  glist->count++;
  prev->next = new;
  
  return EXIT_SUCCESS;
  
}

void GLIListFree(gli_list* glist)
{
  gli_node* next;
  gli_node* curr;
  assert(glist != NULL);
  
  curr = glist->head;
  while (curr != NULL) {
    next = curr->next;
    free(curr);
    curr = next;
  }
  
  free(glist);
}
