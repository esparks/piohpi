#ifndef POP_CLIENTCALLBACKS_H
#define POP_CLIENTCALLBACKS_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/* make one of these, fill out the vars, use to position..*/
typedef struct {
   float event_grid_y;

} gui_vars;


void receive_message(int32_t size, void *data);
void send_messages(FILE *p_out);
void init_callbacks();
void keyDown(int key);
void keyUp(int key);
void redraw(float delta);

#endif

