#ifndef CLIENT_MESSAGES_H
#define CLIENT_MESSAGES_H

#include <stdlib.h>
#include <pthread.h>

#define POP_MSG_BUFFER_SIZE 128

/* A FIFO message queue and message constructors */

typedef struct message_item_s {
   int32_t message_size;
   void    *message_data;
} message_item_t;

typedef struct {
   message_item_t buffer[POP_MSG_BUFFER_SIZE];
   int max_size;
   int size;
   pthread_mutex_t *lock;  
} message_queue_t;

void  init_queue(message_queue_t *queue);
int   add_message(message_queue_t *queue, int32_t message_size, 
                                             void* message_data);
int   add_start_message(message_queue_t *queue);
int   add_clear_triggers_message(message_queue_t *queue);
int   add_ch_mute_msg(message_queue_t *queue, int channel);
int   add_step_flip_msg(message_queue_t *queue, int channel, int step);
int   increase_step_note(message_queue_t *queue, int track, int step);
int   decrease_step_note(message_queue_t *queue, int track, int step);
int   add_quit_message(message_queue_t *queue);
int   add_tempo_change_message(message_queue_t *queue, int change);
int   add_pattern_dec_message(message_queue_t *queue);
int   add_pattern_inc_message(message_queue_t *queue);
int   add_inc_track_note_msg(message_queue_t *queue, int track_no);
int   add_dec_track_note_msg(message_queue_t *queue, int track_no);
int   add_inc_track_octave_msg(message_queue_t *queue, int track_no);
int   add_dec_track_octave_msg(message_queue_t *queue, int track_no);

#endif
