#ifndef POP_WGEOM_H
#define POP_WGEOM_H

#include <stdlib.h>
#include "GLES/gl.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"
#include "geomlist.h"

#define GL_GLEXT_PROTOTYPES

/* This file holds the geometric primitives for drawing to the display */

/* Global Definitions */
/* some of these should be put in cfg files */
/* a general clean up in here is necessary */

#define TERM -999 /* should be deprecated */
#define NUM_LETTERS 76
#define SMALL_TEXT 0.02f
#define TINY_TEXT 0.01f
#define LARGE_TXT 0.3f
#define STEP_X_SIZE 0.07f
#define STEP_Y_SIZE 0.1f
#define BAR_X 2.0
#define BAR_Y STEP_Y_SIZE
#define ACTIVE_COLOR 0.8
#define INACTIVE_COLOR 0.5
#define ACTIVE_MUTE_COLOR 0.4
#define INACTIVE_MUTE_COLOR 0.1

/* offset = ascii value of the first character in the font */
#define FONT_OFFSET 48

/* structure to hold standard object templates */
typedef struct components_s {
   GLfloat *bar;
   GLfloat *bar_color;
   GLfloat *bar_norms;
   GLfloat *step_quad;
   GLfloat *quad_color_active;
   GLfloat *quad_color_inactive;
   GLfloat *quad_color_mute_active;
   GLfloat *quad_color_mute_inactive;
   GLfloat *quad_normals;
} components_t;

/* Interleaved data array, holds coordinates, normals, colours. */

typedef struct PackedGeometry_s {
   GLfloat x, y, z, nx, ny, nz, r, g, b, a;
} PackedGeometry;


/* shape struct */
typedef struct {
   // holds list of draw types, sub-array sizes and index positions
   struct g_list_s* glist;
  
   // holds interleaved vertex and normal data
   PackedGeometry* data;
  
   /* for eventual move to gpu side buffers  */
   GLuint vbuffer;
   GLuint vbytes; // size of vbuffer in bytes

   int polys;
} Geometry;


typedef struct {
  float* sym[NUM_LETTERS];
  GLuint ibuf[NUM_LETTERS];
  PackedGeometry* fontGeom;
  GLuint gbuff;
  /* store the pointers in an array for fast access */
  struct g_data_s* alpha;
} Font;


/* function prototypes */
Font           *makeFont();
components_t   *make_components();

#endif

