/* Pi-Oh-Pi Display Geometry */
#include "pop_wgeom.h"
#include "geomlist.h"
#include <stdio.h>
#include "pop_client.h"
#include <string.h>

/* Terminator for character font definitions */
/*static Font* font;*/


void generateFontData(Font* font);

components_t
*make_components()
{
   components_t   *cmp = malloc(sizeof(components_t));
   size_t         colsize = 16 * sizeof(GLfloat);
   size_t         gsize = 12 * sizeof(GLfloat);
   size_t         nsize = 12 * sizeof(GLfloat);

   /* navbar geometry */
   GLfloat bar[] = {BAR_X, BAR_Y, 0.0,
                    0.0, BAR_Y, 0.0,
                    BAR_X, 0.0, 0.0,
                    0.0, 0.0, 0.0};

   GLfloat bar_color[] = {0.6, 1.0, 0.6, 0.8,
                      0.6, 1.0, 0.6, 0.8,
                      0.6, 1.0, 0.6, 0.8,
                      0.6, 1.0, 0.6, 0.8};

   GLfloat bar_norms[] = {0.0, 0.0, -1.0,
                      0.0, 0.0, -1.0,
                      0.0, 0.0, -1.0,
                      0.0, 0.0, -1.0};

  /* colours for grid steps */
   GLfloat step_quad[] = { STEP_X_SIZE, STEP_Y_SIZE, 0.0,
                            0.0, STEP_Y_SIZE, 0.0,
                            STEP_X_SIZE, 0.0, 0.0,
                            0.0, 0.0, 0.0};
    
   GLfloat quad_color_active[] = { 1.0, 1.0, 1.0, ACTIVE_COLOR,
                                          1.0, 1.0, 1.0, ACTIVE_COLOR,
                                          1.0, 1.0, 1.0, ACTIVE_COLOR,
                                          1.0, 1.0, 1.0, ACTIVE_COLOR};
   
   GLfloat quad_color_inactive[] = { 0.0, 0.0, 0.0, INACTIVE_COLOR,
                                            0.0, 0.0, 0.0, INACTIVE_COLOR,
                                            0.0, 0.0, 0.0, INACTIVE_COLOR,
                                            0.0, 0.0, 0.0, INACTIVE_COLOR};

   GLfloat quad_color_mute_active[] = { 1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR,
                                   1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR,
                                   1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR,
                                   1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR};

   GLfloat quad_color_mute_inactive[] = { 0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR,
                                     0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR,
                                     0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR,
                                     0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR};

   GLfloat quad_normals[] = {0.0, 0.0, -1.0,
                                   0.0, 0.0, -1.0,
                                   0.0, 0.0, -1.0,
                                   0.0, 0.0, -1.0};

   cmp->bar = malloc(gsize);
   memcpy(cmp->bar, bar, gsize);

   cmp->bar_color = malloc(colsize);
   memcpy(cmp->bar_color, bar_color, colsize);

   cmp->bar_norms = malloc(nsize);
   memcpy(cmp->bar_norms, bar_norms, nsize);

   cmp->step_quad = malloc(gsize);
   memcpy(cmp->step_quad, step_quad, gsize);

   cmp->quad_color_active = malloc(colsize);
   memcpy(cmp->quad_color_active, quad_color_active, colsize);

   cmp->quad_color_inactive = malloc(colsize);
   memcpy(cmp->quad_color_inactive, quad_color_inactive, colsize);

   cmp->quad_color_mute_active = malloc(colsize);
   memcpy(cmp->quad_color_mute_active, quad_color_mute_active, colsize);

   cmp->quad_color_mute_inactive = malloc(colsize);
   memcpy(cmp->quad_color_mute_inactive, quad_color_mute_inactive, colsize);

   cmp->quad_normals = malloc(nsize);
   memcpy(cmp->quad_normals, quad_normals, nsize);

   return (cmp);
}


/* Generate the font */
Font* makeFont()
{
  int i;
  Font* font = malloc(sizeof(Font));

  /* crossed out box to use for unsupported characters */
  static float nochar[] = {1, 3, 3, 9, 9, 7, 7, 1, 1, 9, 3, 7, TERM};

  static float n0d[] = {1, 3, 3, 9, 9, 7, 7, 1, TERM};
  font->sym[0] = n0d;

  static float n1d[] = {2, 8, TERM};
  font->sym[1] = n1d;

  static float n2d[] = {4, 8, 8, 9, 9, 6, 6, 1, 1, 3, TERM};
  font->sym[2] = n2d;

  static float n3d[] = {1, 3, 3, 9, 9, 7, 4, 6, TERM};
  font->sym[3] = n3d;

  static float n4d[] = {7, 4, 4, 6, 9, 3, TERM};
  font->sym[4] = n4d;

  static float n5d[] = {9, 7, 7, 4, 4, 6, 6, 3, 3, 1, TERM};
  font->sym[5] = n5d;

  static float n6d[] = {9, 7, 7, 1, 1, 3, 3, 6, 6, 4, TERM};
  font->sym[6] = n6d;

  static float n7d[] = {7, 9, 9, 3, TERM};
  font->sym[7] = n7d;

  static float n8d[] = {7, 9, 9, 3, 3, 1, 1, 7, 4, 6, TERM};
  font->sym[8] = n8d;

  static float n9d[] = {7, 9, 9, 3, 3, 1, 7, 4, 4, 6, TERM};
  font->sym[9] = n9d;

  /* fill unused characters */
  for (i = 0; i < 40; i++)
  {
      font->sym[10 + i] = nochar;
  }


  static float aD[] = {1, 7, 7, 9, 9, 3, 4, 6, TERM};
  font->sym[49] = aD;
  
  static float bD[] = {1, 7, 7, 8, 8, 5, 4, 6, 6, 3, 3, 1, TERM};
  font->sym[50] = bD;
  
  static float cD[] = {9, 7, 7, 1, 1, 3, TERM};
  font->sym[51] = cD;
  
  static float dD[] = {7, 8, 8, 6, 6, 2, 2, 1, 1, 7, TERM};
  font->sym[52] = dD;
  
  static float eD[] = {9, 7, 7, 1, 1, 3, 4, 5, TERM};
  font->sym[53] = eD;
  
  static float fD[] = {9, 7, 7, 1, 4, 5, TERM};
  font->sym[54] = fD;
  
  static float gD[] = {9, 7, 7, 1, 1, 3, 3, 6, 6, 5, TERM};
  font->sym[55] = gD;
  
  static float hD[] = {7, 1, 4, 6, 9, 3, TERM};
  font->sym[56] = hD;
  
  static float iD[] = {7, 9, 8, 2, 1, 3, TERM};
  font->sym[57] = iD;
  
  static float jD[] = {9, 3, 3, 1, 1, 4, TERM};
  font->sym[58] = jD;
  
  static float kD[] = {9, 5, 5, 3, 7, 1, 4, 5, TERM};
  font->sym[59] = kD;
  
  static float lD[] = {7, 1, 1, 3, TERM};
  font->sym[60] = lD;
  
  static float mD[] = {1, 7, 7, 9, 9, 3, 8, 2, TERM};
  font->sym[61] = mD;
  
  static float nD[] = {1, 7, 7, 8, 8, 6, 6, 3, TERM};
  font->sym[62] = nD;
  
  static float oD[] = {1, 7, 7, 9, 9, 3, 3, 1, TERM};
  font->sym[63] = oD;
  
  static float pD[] = {7, 9, 9, 6, 6, 4, 1, 7, TERM};
  font->sym[64] = pD;
  
  static float qD[] = {7, 9, 9, 3, 3, 1, 1, 7, 5, 3, TERM};
  font->sym[65] = qD;
  
  static float rD[] = {1, 7, 7, 9, 9, 5, 5, 3, TERM};
  font->sym[66] = rD;
  
  static float sD[] = {1, 3, 3, 6, 6, 4, 4, 7, 7, 9, TERM};
  font->sym[67] = sD;
  
  static float tD[] = {7, 9, 8, 2, TERM};
  font->sym[68] = tD;
  
  static float uD[] = {7, 1, 1, 3, 3, 9, TERM};
  font->sym[69] = uD;
  
  static float vD[] = {7, 4, 4, 2, 2, 3, 3, 9, TERM};
  font->sym[70] = vD;
  
  static float wD[] = {7, 1, 1, 3, 2, 5, 3, 9, TERM};
  font->sym[71] = wD;
  
  static float xD[] = {7, 3, 9, 1, TERM};
  font->sym[72] = xD;
  
  static float yD[] = {7, 4, 4, 6, 9, 3, 3, 1, TERM};
  font->sym[73] = yD;
  
  static float zD[] = {7, 9, 9, 1, 1, 3, TERM};
  font->sym[74] = zD;
  
  static float bangD[] = {2, 2, 5, 8, TERM};
  font->sym[75] = bangD;

  /* now stuff the data into the struct */
  generateFontData(font);
  return (font); 
}

void generateFontData(Font* font)
{
   /* Generates the point grid for letters and 
   puts the index lists in the alpha[] array. */
   
   int i, n;
   
   GLushort* ind;
   
   /* default color val white */
   GLfloat col[] = {1.0, 1.0, 1.0, 1.0};

   /* default normal data empty */
   GLfloat norm[] = {0.0, 0.0, 0.0, 0.0};

   PackedGeometry *pg;
   
   /* generate the point grid */
   font->fontGeom = malloc(sizeof(PackedGeometry) * 9);
   font->alpha = malloc(sizeof(g_data) * NUM_LETTERS);
    
   pg = font->fontGeom;

   pg->x = 0;
   pg->y = 0;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 1;
   pg->y = 0;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 2;
   pg->y = 0;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 0;
   pg->y = 1;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 1;
   pg->y = 1;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 2;
   pg->y = 1;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 0;
   pg->y = 2;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 1;
   pg->y = 2;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   pg++;

   pg->x = 2;
   pg->y = 2;
   pg->z = 0;
   pg->nx = norm[0];
   pg->ny = norm[1];
   pg->nz = norm[2];
   pg->r = col[0];
   pg->b = col[1];
   pg->g = col[2];
   pg->a = col[3];
   
   glGenBuffers(NUM_LETTERS, font->ibuf);
   glGenBuffers(1, &font->gbuff);
   glBindBuffer(GL_ARRAY_BUFFER, font->gbuff);
   glBufferData(GL_ARRAY_BUFFER, sizeof(PackedGeometry) * 9, font->fontGeom,
                GL_STATIC_DRAW);
   glBindBuffer(GL_ARRAY_BUFFER, 0);

   /* now fill out the gdata items */
   /* NUM_LETTERS doesn't match defined letters.... */
   for(i = 0; i < NUM_LETTERS; i++)
   {
     
      /* drop terminator. should remove since is deprecated */
      font->alpha[i].vertices = 0;
 
      /* build the index */

      font->alpha[i].indices = malloc(sizeof(GLushort) * 100);
      font->alpha[i].drawMode = GL_LINES;

      ind = font->alpha[i].indices;
      n = 0;
      
      while (font->sym[i][n] != TERM) {
         *ind = (GLushort)font->sym[i][n] - 1;
         ind++;
         font->alpha[i].vertices++;
         n++;   
      }
     
      ind = realloc(font->alpha[i].indices, sizeof(GLushort) * font->alpha[i].vertices);
      font->alpha[i].indices = ind;
      
      /* bind the index data */
      /* buffered data is running slowly. needs further testing */
      /*
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, font->ibuf[i]);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * font->alpha[i].vertices,
                  font->alpha[i].indices, GL_STATIC_DRAW);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);*/
   }
}

 
