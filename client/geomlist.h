#ifndef GEOMLIST_H
#define GEOMLIST_H

struct PackedGeometry; // forward declaration

#include "pop_wgeom.h"
#include "GLES/gl.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"


#define GL_GLEXT_PROTOTYPES

/* Linked list of vertex counts, indices, index buffers, drawmode etc. */
/* List is unsorted. */

typedef struct g_data_s {
  int vertices;
  GLushort* indices;
  GLuint indexBuffer;
  GLuint drawMode;
  GLuint vbuffer;
} g_data;

typedef struct t_node {
  g_data* data;
  struct t_node* next;
} t_node;

typedef struct g_list_s {
  int count;
  struct t_node* head;
} g_list;

/* Linked List of GLuints for storing buffer references */
typedef struct gli_node {
  GLuint value;
  struct gli_node* next;
} gli_node;

typedef struct gli_list {
  int count;
  struct gli_node* head;
} gli_list;

void GListInit(g_list*);
int  GListInsert(g_list*, g_data*);
void GListFree(g_list*);

void GLIListInit(gli_list*); // list of GLuints for buffers &c.
int GLIListInsert(gli_list*, GLuint);
void GLIListFree(gli_list*);


#endif
