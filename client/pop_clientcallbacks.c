/* Main client callbacks file
 * pop_client.c calls functions in here
 */

#include <SDL/SDL.h>
#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "GLES/gl.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"
#include "pop_wgeom.h"
#include "geomlist.h"
#include "pop_client.h"
#include "../common/pattern.h"
#include "../common/common_pipe.h"
#include "client_messages.h"
#include "pop_clientcallbacks.h"

#define SHOW_KEY_HINTS 0

/*Data structures to hold gui info*/
typedef struct {
   GLfloat           bg_color[4];
   Font*             font;
} Graphics;


typedef struct {
   Graphics          graphics;
   pattern_t         *pattern;
   int               track_no;
   int               step_no;
   components_t      *components;
   gui_vars          gvars;
} gui_t;

static gui_t *gui;
static message_queue_t  messages;

/* we don't have a solution to show sharps and flats yet as the font engine
   cannot render them currently */
static char             *notes[12] = {"c", "c", "d", "d", "e", "f", "f", "g", 
                                      "g", "a", "a", "b"};

/* function prototypes */
void drawGeomObject(Geometry* obj);
void renderText(char* text, GLfloat fontSize);
void renderGridStep(event_t *event, int mute);
void renderStepBar();
void renderTrackSelection();
void change_track_selection(int change);
void renderSelectionBox();
void change_step_selection(int change);

void 
change_step_selection(int change)
{
   gui->step_no += change;
   if (gui->step_no < 0)
      gui->step_no = EVENTS_PER_TRACK - 1;

   if (gui->step_no >= EVENTS_PER_TRACK)
      gui->step_no = 0;
}

void 
change_track_selection(int change)
{
   gui->track_no += change;
   if (gui->track_no < 0)
      gui->track_no = MAX_TRACKS - 1;

   if (gui->track_no >= MAX_TRACKS)
      gui->track_no = 0;
}

void 
send_messages(FILE* p_out)
{
   int i;
   
   pthread_mutex_lock(messages.lock);
 
   if (messages.size < 1) {
      pthread_mutex_unlock(messages.lock);
      return;
   }

   /* perform writes individually */
   /* would single write make a difference? */
   for (i = 0; i < messages.size; i++) {
      int32_t size = messages.buffer[i].message_size;
      fwrite(&size, sizeof(int32_t), 1, p_out);
      fwrite(messages.buffer[i].message_data, 1, size, p_out);
      fflush(p_out);
      messages.buffer[i].message_size = 0;
      free(messages.buffer[i].message_data);
   }

   messages.size = 0;
   pthread_mutex_unlock(messages.lock);

}

/* only we write to the pattern */
void 
receive_message(int32_t message_size, void* data)
{
   /* first byte of void is message type, remaining bytes are params
    * or more complex types like a clone of a pattern object */
   
   if(!gui->pattern) {
      /*free(pattern);*/
      gui->pattern = malloc(message_size);
   }

   gui->graphics.bg_color[2] = 0.8;
   memcpy(gui->pattern, data, message_size);
   free(data);
}

void 
init_callbacks()
{
   printf("initing calllbacks....\n");
   /* needs error checking, should return int */
   gui = malloc(sizeof(gui_t));
   /* A lot of defines from wgeom should move into guivars
    * and be read from a config file */
   gui->gvars.event_grid_y = 0.6;
   gui->track_no = 0;
   gui->step_no = 0;

   /* make the font and tile data */

   gui->components = make_components();
   gui->graphics.font = makeFont();
  
   /* default screen color with no server running */
   gui->graphics.bg_color[0] = 0.3f; /* r */
   gui->graphics.bg_color[1] = 0.0f; /* g */
   gui->graphics.bg_color[2] = 0.0f; /* b */
   gui->graphics.bg_color[3] = 1.0f; /* a */

   glClearColor(gui->graphics.bg_color[0],
                gui->graphics.bg_color[1],
                gui->graphics.bg_color[2],
                gui->graphics.bg_color[3]);

   /* opengl flags initial state */
   glEnable(GL_CULL_FACE);
   glMatrixMode(GL_MODELVIEW);

   /* set up messages */
   init_queue(&messages);

   gui->pattern = NULL;
}

/* Redraw the display. Delta passed in, in case we want to do smooth
   animation */
void 
redraw(float delta)
{
   static int  last_time = 0;
   static int  update = 50;
   static int  u = 0;
   static int  sdelta = 0;
   int         step = 0;
   int         time = SDL_GetTicks();
   int         d = time - last_time;
   int         i = 0;
   int         j = 0;
   char        text[32];
   char        bpm[32];
   char        patno[12];
   float       bar_x_mod = 0.01;
   float       bar_offset = -0.5;
   float       bpm_x = 0.48;
   float       bpm_y = 0.9;
   float       pattern_x = -0.48;
   float       cell_height = 0.11;
   float       track_labels_x = -0.9;
   float       track_selection_x = -1.0;
   float       step_x_mod = 0.01;
#if SHOW_KEY_HINTS
   char        kb_labels[16] = {'q','w','e','r','t','y','u','i',
                                'a','s','d','f','g','h','j','k'};
#endif

   last_time = time;   
   u++;

   if (u >= update) {
      sdelta = d;
      u = 0;
   }

   if (gui->pattern != NULL) { 
      step = gui->pattern->curr_step; 
   }
//      step = gui->pattern->curr_step; 

   glClearColor(gui->graphics.bg_color[0], 
                gui->graphics.bg_color[1],
                gui->graphics.bg_color[2],
                gui->graphics.bg_color[3]);

   sprintf(text, "fms %d", sdelta);

   if (gui->pattern != NULL) {
      sprintf(bpm, "%d bpm", gui->pattern->tempo);
      sprintf(patno, "pattern %d", gui->pattern->id);
   } else {
      sprintf(bpm, "no server");
      sprintf(patno, " ");
   }

   glLoadIdentity();
   glClear(GL_COLOR_BUFFER_BIT);
  
   /* draw big highlight bars */
   glPushMatrix();
   glTranslatef(bar_offset + ((step + 1) * 
                 (STEP_X_SIZE + bar_x_mod)), 0.0, 0.1);
   renderStepBar();
   glPopMatrix();

   /* UNCOMMENT TO SHOW FRAME TIME*/
   glPushMatrix();
   glTranslatef(-0.9, 0.9, 0.3);
   renderText(text, SMALL_TEXT);
   glPopMatrix();
   /**/  

   /* Beats Per Minute */
   glPushMatrix();
   glTranslatef(bpm_x + STEP_X_SIZE, bpm_y, 0.1);
   renderText(bpm, SMALL_TEXT);
   glPopMatrix();

   /* Pattern Number */
   glPushMatrix();
   glTranslatef(pattern_x + STEP_X_SIZE, bpm_y, 0.1);
   renderText(patno, SMALL_TEXT);
   glPopMatrix();

   /* Track selection bar */
   glPushMatrix();
   glTranslatef(track_selection_x, 
                gui->gvars.event_grid_y - (gui->track_no *
                cell_height), 0.3);
   renderTrackSelection();
   glPopMatrix();

   /* Render events grid */
   for (i = 0; i < MAX_TRACKS ; i++) {

      glPushMatrix();
      
      glTranslatef(track_labels_x, 
                   gui->gvars.event_grid_y - (i * cell_height), 0.0);

      /* Hard-coded track labels: maybe can pull these from pattern at some
         point */
	   switch (i) {
		   case 0:
			   renderText("1 kick", SMALL_TEXT);
			   break;
		   case 1:
			   renderText("2 bass", SMALL_TEXT);
			   break;
		   case 2:
			   renderText("3 c hat", SMALL_TEXT);
			   break;
		   case 3:
			   renderText("4 clap", SMALL_TEXT);
			   break;
		   case 4:
			   renderText("5 c bell", SMALL_TEXT);
			   break;
		   case 5:
			   renderText("6 ride", SMALL_TEXT);
			   break;
		   case 6:
			   renderText("7 synth", SMALL_TEXT);
			   break;
		   case 7:
			   renderText("8 synth", SMALL_TEXT);
			   break;
		   default:
			   renderText("", SMALL_TEXT);
			   break;
	   }
	   
      glPopMatrix();
 
      glPushMatrix();
      glTranslatef(-0.5, gui->gvars.event_grid_y - (i * cell_height), 0.0); 
      for (j = 0; j < EVENTS_PER_TRACK; j++) {
         glTranslatef(STEP_X_SIZE + step_x_mod, 0, 0);
         if (gui->pattern != NULL) {
            if (gui->track_no == i && gui->step_no == j) {
               renderSelectionBox();
            }
            event_t* event = &gui->pattern->tracks[i].step[j];
            renderGridStep(event, gui->pattern->mutes[i]);
         }
      }
      glPopMatrix();
   }
   return;
/* Shows keyboard mappings above step columns */
/* disabling improves performance */
#if SHOW_KEY_HINTS
   glPushMatrix();
   glTranslatef(-0.48, 0.83, 0.0);
   for (j = 0; j < EVENTS_PER_TRACK; j++) {
      glPushMatrix();
      glTranslatef((j + 1) * (STEP_X_SIZE + 0.01) , 0, 0);
      sprintf(c, "%c", kb_labels[j]);
      
      renderText(c, SMALL_TEXT);
      glPopMatrix();
   }   
   glPopMatrix();
#endif

}

void 
keyDown(int key)
{
   /* Start, Stop, Quit &c. */
   /* --------------------- */
   if (key == SDLK_ESCAPE) {
      add_quit_message(&messages);
      cleanup();
   }

   if (key == SDLK_RETURN)
      add_start_message(&messages);

   if (key == SDLK_DELETE)
      add_clear_triggers_message(&messages);


   /* Track Mute Controls */
   /* ------------------- */
   if (key == SDLK_1)
      add_ch_mute_msg(&messages, 0);

   if (key == SDLK_2)
      add_ch_mute_msg(&messages, 1);

   if (key == SDLK_3)
      add_ch_mute_msg(&messages, 2);

   if (key == SDLK_4)
      add_ch_mute_msg(&messages, 3);

   if (key == SDLK_5)
      add_ch_mute_msg(&messages, 4);

   if (key == SDLK_6)
      add_ch_mute_msg(&messages, 5);

   if (key == SDLK_7)
      add_ch_mute_msg(&messages, 6);

   if (key == SDLK_8)
      add_ch_mute_msg(&messages, 7);

   if (key == SDLK_9)
      add_ch_mute_msg(&messages, 8);

   if (key == SDLK_0)
      add_ch_mute_msg(&messages, 9);

   /* GUI Navigation */
   /* -------------- */
   if (key == SDLK_UP)
      change_track_selection(-1);

   if (key == SDLK_DOWN)
      change_track_selection(1);

   if (key == SDLK_RIGHT)
      change_step_selection(1);

   if (key == SDLK_LEFT)
      change_step_selection(-1);

   /* step switches */
   /* ------------- */
   if (key == SDLK_SPACE)
      add_step_flip_msg(&messages, gui->track_no, gui->step_no);

   if (key == SDLK_q)
      add_step_flip_msg(&messages, gui->track_no, 0);

   if (key == SDLK_w)
      add_step_flip_msg(&messages, gui->track_no, 1);

   if (key == SDLK_e)
      add_step_flip_msg(&messages, gui->track_no, 2);

   if (key == SDLK_r)
      add_step_flip_msg(&messages, gui->track_no, 3);

   if (key == SDLK_t)
      add_step_flip_msg(&messages, gui->track_no, 4);
   
   if (key == SDLK_y)
      add_step_flip_msg(&messages, gui->track_no, 5);

   if (key == SDLK_u)
      add_step_flip_msg(&messages, gui->track_no, 6);

   if (key == SDLK_i)
      add_step_flip_msg(&messages, gui->track_no, 7);

   if (key == SDLK_a)
      add_step_flip_msg(&messages, gui->track_no, 8);

   if (key == SDLK_s)
      add_step_flip_msg(&messages, gui->track_no, 9);

   if (key == SDLK_d)
      add_step_flip_msg(&messages, gui->track_no, 10);

   if (key == SDLK_f)
      add_step_flip_msg(&messages, gui->track_no, 11);

   if (key == SDLK_g)
      add_step_flip_msg(&messages, gui->track_no, 12);

   if (key == SDLK_h)
      add_step_flip_msg(&messages, gui->track_no, 13);

   if (key == SDLK_j)
      add_step_flip_msg(&messages, gui->track_no, 14);

   if (key == SDLK_k)
      add_step_flip_msg(&messages, gui->track_no, 15);

   /* Tempo controls */
   /* -------------- */
   if (key == SDLK_EQUALS)
      add_tempo_change_message(&messages, 1);

   if (key == SDLK_MINUS)
      add_tempo_change_message(&messages, -1);

   /* Pattern change controls */
   /* ----------------------- */
   if (key == SDLK_PAGEUP)
      add_pattern_inc_message(&messages);

   if (key == SDLK_PAGEDOWN)
      add_pattern_dec_message(&messages);

   /* Note value modifications */
   /* ------------------------ */
   if (key == SDLK_m)
      add_inc_track_note_msg(&messages, gui->track_no);

   if (key == SDLK_n)
      add_dec_track_note_msg(&messages, gui->track_no);

   if (key == SDLK_QUOTE)
      add_inc_track_octave_msg(&messages, gui->track_no);

   if (key == SDLK_SEMICOLON)
      add_dec_track_octave_msg(&messages, gui->track_no);

   if (key == SDLK_COMMA)
      decrease_step_note(&messages, gui->track_no, gui->step_no);

   if (key == SDLK_PERIOD)
      increase_step_note(&messages, gui->track_no, gui->step_no);

}

void
keyUp(int key)
{
   /* NOT IMPLEMENTED */
}


void renderText(char* text, GLfloat fontSize)
{
   char* c;
   c = text;
   int len = strlen(text);
   int i;
    
   int stride = sizeof(GLfloat) * 10;
   GLfloat* verts = (GLfloat*)gui->graphics.font->fontGeom;
   GLfloat* norms = (GLfloat*)gui->graphics.font->fontGeom;
   GLfloat* color = (GLfloat*)gui->graphics.font->fontGeom;
   norms += 3;
   color += 6;

   glPushMatrix();
   glScalef(fontSize, fontSize, fontSize);
   
   glVertexPointer(3, GL_FLOAT, stride, verts);
   glNormalPointer(GL_FLOAT, stride, norms);
   glColorPointer(4, GL_FLOAT, stride, color);

   for(i = 0; i < len; i++)
   {
      /*if (*c < 48)
         *c = 48;

      if (*c > 122)
         *c = 122;*/

      g_data* curr = &gui->graphics.font->alpha[*c - FONT_OFFSET];
      glDrawElements(curr->drawMode, curr->vertices, GL_UNSIGNED_SHORT,
                     curr->indices);
      glTranslatef(3.0, 0.0, 0.0);
      c++;
   }
   glPopMatrix();
}

/* draws the interactive cursor         */
/* data needs moving to gui->components */
void renderSelectionBox()
{
   glPushMatrix();
   glTranslatef(-STEP_X_SIZE * 0.25, -STEP_Y_SIZE * 0.25, 0.5);

   float x_size = STEP_X_SIZE * 1.3;
   float y_size = STEP_Y_SIZE * 1.3;

   GLfloat quad[] = { x_size, y_size, 0.0,
                      0.0,    y_size, 0.0,
                      x_size, 0.0,    0.0,
                      0.0,    0.0,    0.0};

   GLfloat select_color[] = { 1.0, 1.0, 0.0, 1.0,
                              1.0, 1.0, 0.0, 1.0,
                              1.0, 1.0, 0.0, 1.0,
                              1.0, 1.0, 0.0, 1.0};

   GLfloat select_norms[] = { 0.0, 0.0, -1.0,
                              0.0, 0.0, -1.0,
                              0.0, 0.0, -1.0,
                              0.0, 0.0, -1.0};
   
   glVertexPointer(3, GL_FLOAT, 0, quad);
   glNormalPointer(GL_FLOAT, 0, select_norms);
   glColorPointer(4, GL_FLOAT, 0, select_color);
   glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
   glPopMatrix();
}

void renderGridStep(event_t *event, int mute)
{
   glVertexPointer(3, GL_FLOAT, 0, gui->components->step_quad);
   glNormalPointer(GL_FLOAT, 0, gui->components->quad_normals);
   if (!mute) {
      if (event->trigger)
         glColorPointer(4, GL_FLOAT, 0, gui->components->quad_color_active);
      else
         glColorPointer(4, GL_FLOAT, 0, gui->components->quad_color_inactive);
   } else {
     if (event->trigger)
         glColorPointer(4, GL_FLOAT, 0.0, gui->components->quad_color_mute_active);
     else
         glColorPointer(4, GL_FLOAT, 0.0, gui->components->quad_color_mute_inactive);
   }
   glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
   renderText(notes[event->note_val % 12] , TINY_TEXT);
  
}

void renderStepBar()
{
   /* needs moving to components struct */
   float x_size = STEP_X_SIZE;
   float y_size = 1.0f;

   GLfloat sbar[] = {x_size, y_size, 0.0,
                           0.0, y_size, 0.0,
                           x_size, -y_size, 0.0,
                           0.0, -y_size, 0.0};
   GLfloat sbarcolor[] = { 0.6, 0.6, 1.0, 1.0,
                                 0.6, 0.6, 1.0, 1.0,
                                 0.6, 0.6, 1.0, 1.0,
                                 0.6, 0.6, 1.0, 1.0};

   GLfloat sbarnorms[] = { 0.0, 0.0, -1.0,
                                 0.0, 0.0, -1.0,
                                 0.0, 0.0, -1.0,
                                 0.0, 0.0, -1.0};

   glVertexPointer(3, GL_FLOAT, 0, sbar);
   glNormalPointer(GL_FLOAT, 0, sbarnorms);
   glColorPointer(4, GL_FLOAT, 0, sbarcolor);
   glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}

void renderTrackSelection()
{
   glVertexPointer(3, GL_FLOAT, 0, gui->components->bar);
   glNormalPointer(GL_FLOAT, 0, gui->components->bar_norms);
   glColorPointer(4, GL_FLOAT, 0, gui->components->bar_color);
   glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}

/* draws a generic object
 *  currently unused
 */  
void drawGeomObject(Geometry* obj)
{
   /* iterate through obj's glist, drawing all elements */
   g_list* glist = obj->glist;
   t_node* curr = glist->head;
   int floats_per_vertex = 6;
  
   /* stride length for packed verts/norms array */
   int stride = sizeof(GLfloat) * floats_per_vertex;
  
   //GLint currBuffer = 0;
  
   if (curr == NULL) {
      printf("attempted to draw empty object\n");
      return;
   }
  
   /* if buffer bound, clear it*/
   /* Buffers not implemented 
   glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &currBuffer);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
   if (currBuffer != 0) {
   glBindBuffer(GL_ARRAY_BUFFER, 0);
   }*/

   GLfloat* verts = (GLfloat*)obj->data;
   GLfloat* norms = (GLfloat*)obj->data;
   norms += (floats_per_vertex / 2);
  
   while (curr != NULL){
      glVertexPointer(3, GL_FLOAT, stride, verts);
      glNormalPointer(GL_FLOAT, stride, norms);
      glDrawElements(curr->data->drawMode, curr->data->vertices, 
                    GL_UNSIGNED_SHORT, curr->data->indices);

      curr = curr->next;
   }
}

