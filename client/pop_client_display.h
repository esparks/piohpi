#ifndef POP_CLIENT_DISPLAY_H
#define POP_CLIENT_DISPLAY_H

#include <assert.h>
#include <stdlib.h>
#include "bcm_host.h"
#include <stdio.h>
#include "SDL/SDL.h"
#include "GLES/gl.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"


/* hold display data */
typedef struct {
   uint32_t    width;
   uint32_t    height;
   EGLDisplay  display;
   EGLSurface  surface;
   EGLContext  context;
} SCREEN_T;

void init_ogl(SCREEN_T *screen);
void init_projection(SCREEN_T *screen);


#endif

