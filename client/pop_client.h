#ifndef POP_CLIENT_H
#define POP_CLIENT_H

#define POP_CLIENT_MAX_MESSAGES 128

typedef struct {
   unsigned char  **messages;
   unsigned char  *message_sizes;
   int            num_messages;
} message_queue;


void cleanup();
void quit();
#endif

