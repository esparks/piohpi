/* Testing input using SDL on rasperry pi */

#include <assert.h>
#include <stdlib.h>
#include "bcm_host.h"
#include <stdio.h>
#include "SDL/SDL.h"
#include "pop_client.h"
#include "pop_client_display.h"
#include "pop_clientcallbacks.h"
#include "../common/common_pipe.h"
#include "../common/pop_utils.h"
#include <math.h>


/*OpenGL ES includes */
#include "GLES/gl.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"

#define TRAP_START_ERRORS 0

/* global quit control */
static int quitFlag = 0;

/* Thread to run sending of messages to the server 
 * send_messages will attempt to acquire a mutex lock on 
 * the message queue, if successfull sends all available messages
 * and then goes to sleep for a short time to avoid spamming the lock */
 
void*
run_pipe_output()
{
   FILE* p_out;
   p_out = fopen(PIOH_PIPE_IN, "wb");
 
   while(!quitFlag) {
      send_messages(p_out);
      pop_nan_nap(1000);
   }

   fclose(p_out);
   return(NULL);
}

/* Handles input from the server. reads the size of chunk and the chunk,
 * then passes it to the receive_message callback which will take a copy */
void*
run_pipe_input()
{
   FILE     *p_in;
   int32_t  size = 0;
   void     *data;
   
   p_in = fopen(PIOH_PIPE_OUT, "rb");
   
   if (p_in == NULL) {
      printf("Error opening client pipe input: %d", errno);
   }
   
   while(!quitFlag) {
      fread(&size, sizeof(int32_t), 1, p_in);
      data = malloc(size);
      fread(data, size, 1, p_in);
      receive_message(size, data);
      pop_nan_nap(500);
   }

   fclose(p_in);
   return(NULL);
}

void
cleanup()
{
   quitFlag = 1;
   SDL_Quit();
   bcm_host_deinit();
}

void 
reshape(int w, int h)
{
   /* not needed */
}

int
main(int argc, char** argv)
{
   pthread_t         thread_pipe_out;
   pthread_t         thread_pipe_in;
   int               p_err; 
   uint32_t          ticks;
   uint32_t          lastTicks;
   float             delta;
   SCREEN_T          *display=malloc(sizeof(SCREEN_T));

   bcm_host_init();
 
   /* init sdl */

   SDL_Init(SDL_INIT_VIDEO |SDL_INIT_TIMER); /*       no video yet */
   SDL_Event event;  

   /* creating a surface gives us access to SDL's input methods, 
      but init'ing with size 0 causes stall on some monitors. 
      Try display->width, display->height or 1, 1 if you
      encounter errors */

   /*SDL_Surface* window = SDL_SetVideoMode(display->width, display->height, 32,
                                                               SDL_SWSURFACE);*/
   /* init opengl */

   memset(display, 0, sizeof(*display));

   init_ogl(display);
   init_projection(display);
   init_callbacks();

   SDL_Surface *window = SDL_SetVideoMode(1, 
                                          1, 
                                          32,
                                          SDL_HWSURFACE);

   if (window == NULL) {
      printf("error opening surface dimensions %d, %d\n", display->width,
                                                          display->height);
      cleanup();
   }


#if TRAP_START_ERRORS
   /* allows printf debugging of init functions */
   bcm_host_deinit();
   exit(EXIT_SUCCESS);
#endif

   lastTicks = SDL_GetTicks();

   /* Open the pipes */
   p_err = pthread_create(&thread_pipe_out, NULL, run_pipe_output, NULL);

   if (p_err != 0) {
      fprintf(stderr, "Couldn't create thread to run pipe out\n");
      cleanup();
      return(EXIT_FAILURE);
   }
   
   p_err = pthread_create(&thread_pipe_in, NULL, run_pipe_input, NULL);

   if (p_err != 0) {
      fprintf(stderr, "Couldn't create thread to run pipe in\n");
      cleanup();
      return(EXIT_FAILURE);
   }

   while(!quitFlag){
   
      ticks = SDL_GetTicks();
      delta = ticks - lastTicks;
      lastTicks = ticks;

      while (SDL_PollEvent(&event)) {
         switch(event.type) { 
            case SDL_QUIT:
               cleanup();
               break;
            case SDL_KEYDOWN:
               keyDown(event.key.keysym.sym);
               break;
           default:
               break;
         }
      }

      redraw(delta);
      eglSwapBuffers(display->display, display->surface);
   } 
   cleanup();
   return(EXIT_SUCCESS);
}


