#include "pop_client.h"
#include "client_messages.h"
#include "../common/common_pipe.h"
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <assert.h>

void
init_queue(message_queue_t *queue)
{
   /* set up the lock and other things */
   queue->lock = malloc(sizeof(pthread_mutex_t));
   int mut_err = pthread_mutex_init(queue->lock, NULL);
   assert(mut_err == 0);

   queue->max_size = POP_MSG_BUFFER_SIZE; 
}


int
add_message(message_queue_t *queue, int32_t size, void *data)
{
   pthread_mutex_lock(queue->lock);

   if (queue->size < queue->max_size) {
      queue->buffer[queue->size].message_size = size;
      queue->buffer[queue->size].message_data = data;
      queue->size++;
      pthread_mutex_unlock(queue->lock);
      return(1);
   }

   pthread_mutex_unlock(queue->lock);
   return(0);
}


int 
add_ch_mute_msg(message_queue_t *queue, int channel)
{
   int32_t message_size = 2;
   unsigned char* data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_MUTE_CHANNEL_1;
   data[1] = (unsigned int)channel;
   return (add_message(queue, message_size, data));
}

int add_step_flip_msg(message_queue_t *queue, int channel, int step)
{
   int32_t message_size = 3;
   unsigned char* data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_FLIP_STEP_TRIGGER;
   data[1] = (unsigned int)channel;
   data[2] = (unsigned int)step;
   return (add_message(queue, message_size, data));
}

int
add_start_message(message_queue_t *queue)
{
   int32_t message_size = 1;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_START_PLAYBACK;
   return (add_message(queue, message_size, data));
}

int 
add_clear_triggers_message(message_queue_t *queue)
{
   int32_t message_size = 1;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_CLEAR_ALL_TRIGGERS;
   return (add_message(queue, message_size, data));
}

int 
increase_step_note(message_queue_t *queue, int track_no, int step_no)
{
   int32_t message_size = 3;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_INC_STEP_NOTE;
   data[1] = (unsigned char)track_no;
   data[2] = (unsigned char)step_no;
   return (add_message(queue, message_size, data));
}

int
decrease_step_note(message_queue_t *queue, int track_no, int step_no)
{
   int32_t message_size = 3;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_DEC_STEP_NOTE;
   data[1] = (unsigned char)track_no;
   data[2] = (unsigned char)step_no;
   return (add_message(queue, message_size, data));
}

int
add_quit_message(message_queue_t *queue)
{
	int32_t message_size = 1;
	unsigned char *data = malloc(sizeof(unsigned char) * message_size);
	data[0] = POP_EXIT_PROGRAM;
	return(add_message(queue, message_size, data));
}

int 
add_tempo_change_message(message_queue_t *queue, int change)
{
   int32_t message_size = 2;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_CHANGE_TEMPO;
   data[1] = (signed char) change;
   return(add_message(queue, message_size, data));
}

int
add_pattern_inc_message(message_queue_t *queue)
{
   int32_t message_size = 1;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_PATTERN_UP;
   return (add_message(queue, message_size, data));
}

int
add_pattern_dec_message(message_queue_t *queue)
{
   int32_t message_size = 1;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_PATTERN_DOWN;
   return (add_message(queue, message_size, data));
}

int
add_inc_track_note_msg(message_queue_t *queue, int track_no)
{
   int32_t message_size = 2;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_INC_TRACK_NOTE;
   data[1] = (unsigned char)track_no;
   return (add_message(queue, message_size, data));
}

int 
add_dec_track_note_msg(message_queue_t *queue, int track_no)
{
   int32_t message_size = 2;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_DEC_TRACK_NOTE;
   data[1] = (unsigned char)track_no;
   return (add_message(queue, message_size, data));
}

int
add_inc_track_octave_msg(message_queue_t *queue, int track_no)
{
   int32_t message_size = 2;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_INC_TRACK_OCTAVE;
   data[1] = (unsigned char)track_no;
   return (add_message(queue, message_size, data));
}

int 
add_dec_track_octave_msg(message_queue_t *queue, int track_no)
{
   int32_t message_size = 2;
   unsigned char *data = malloc(sizeof(unsigned char) * message_size);
   data[0] = POP_DEC_TRACK_OCTAVE;
   data[1] = (unsigned char)track_no;
   return (add_message(queue, message_size, data));
}
