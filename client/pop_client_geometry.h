#ifndef POP_CLIENT_GEOMETRY_H
#define POP_CLIENT_GEOMETRY_H

#include "GLES/gl.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"



#define SMALL_TEXT 0.02f
#define TINY_TEXT 0.01f
#define LARGE_TXT 0.3f
#define STEP_X_SIZE 0.07f
#define STEP_Y_SIZE 0.1f
#define BAR_X 2.0
#define BAR_Y STEP_Y_SIZE
#define ACTIVE_COLOR 0.8
#define INACTIVE_COLOR 0.5
#define ACTIVE_MUTE_COLOR 0.4
#define INACTIVE_MUTE_COLOR 0.1

GLfloat bar[] = {BAR_X, BAR_Y, 0.0,
                    0.0, BAR_Y, 0.0,
                    BAR_X, 0.0, 0.0,
                    0.0, 0.0, 0.0};

GLfloat barcolor[] = {0.6, 1.0, 0.6, 0.8,
                      0.6, 1.0, 0.6, 0.8,
                      0.6, 1.0, 0.6, 0.8,
                      0.6, 1.0, 0.6, 0.8};

GLfloat barnorms[] = {0.0, 0.0, -1.0,
                      0.0, 0.0, -1.0,
                      0.0, 0.0, -1.0,
                      0.0, 0.0, -1.0};

/* colours for grid steps */
   GLfloat step_quad[] = { STEP_X_SIZE, STEP_Y_SIZE, 0.0,
                            0.0, STEP_Y_SIZE, 0.0,
                            STEP_X_SIZE, 0.0, 0.0,
                            0.0, 0.0, 0.0};
    
GLfloat quad_color_active[] = { 1.0, 1.0, 1.0, ACTIVE_COLOR,
                                          1.0, 1.0, 1.0, ACTIVE_COLOR,
                                          1.0, 1.0, 1.0, ACTIVE_COLOR,
                                          1.0, 1.0, 1.0, ACTIVE_COLOR};
   
GLfloat quad_color_inactive[] = { 0.0, 0.0, 0.0, INACTIVE_COLOR,
                                            0.0, 0.0, 0.0, INACTIVE_COLOR,
                                            0.0, 0.0, 0.0, INACTIVE_COLOR,
                                            0.0, 0.0, 0.0, INACTIVE_COLOR};

GLfloat quad_color_mute_active[] = { 1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR,
                                   1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR,
                                   1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR,
                                   1.0, 1.0, 1.0, ACTIVE_MUTE_COLOR};

GLfloat quad_color_mute_inactive[] = { 0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR,
                                     0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR,
                                     0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR,
                                     0.0, 0.0, 0.0, INACTIVE_MUTE_COLOR};

GLfloat quad_normals[] = {0.0, 0.0, -1.0,
                                   0.0, 0.0, -1.0,
                                   0.0, 0.0, -1.0,
                                   0.0, 0.0, -1.0};


#endif


