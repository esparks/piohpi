#ifndef COMMON_PIPE_H
#define COMMON_PIPE_H

/* 
 * Common data types, enumerations and filenames
 * for using the system pipe.
 *
 *
 */

#define PIOH_PIPE_IN  "../pipe/piohpipe_in"
#define PIOH_PIPE_OUT "../pipe/piohpipe_out"

/* Definitions */

#define POP_START_PLAYBACK       0x01
#define POP_STOP_PLAYBACK        0xFF /*deprecated*/
#define POP_EXIT_PROGRAM	      0xF0
#define POP_RESET_PLAYBACK       0x02
#define POP_CHANGE_TEMPO         0x03
#define POP_PATTERN_UP           0x04
#define POP_PATTERN_DOWN         0x05
#define POP_GOTO_PATTERN         0x06  /* for jumping directly */

#define POP_MUTE_CHANNEL_1       0x11
#define POP_MUTE_CHANNEL_2       0x12

#define POP_FLIP_STEP_TRIGGER    0x20
#define POP_CLEAR_ALL_TRIGGERS   0x21
#define POP_INC_STEP_NOTE        0x22
#define POP_DEC_STEP_NOTE        0x23
#define POP_INC_TRACK_NOTE       0x24
#define POP_DEC_TRACK_NOTE       0x25
#define POP_INC_TRACK_OCTAVE     0x26
#define POP_DEC_TRACK_OCTAVE     0x27

#define POP_PATTERN_OBJECT       0xA0


#endif

