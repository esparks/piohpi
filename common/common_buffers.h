#ifndef COMMON_BUFFERS_H
#define COMMON_BUFFERS_H

#include <stdlib.h>



typedef struct {
   int16_t** buffers;
   unsigned char num_buffers;
   int samples;
} buffer_type_t;


#endif

