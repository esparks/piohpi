#include "wave_loader.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void
show_bytes(const char* fname)
{
   FILE* input;
   unsigned char byte;
   int count = 0;
   input = fopen(fname, "r");

   while(!feof(input) && count++ < 72)
   {
      fread(&byte, 1, 1, input);
      printf("%2x %c\n", byte, byte);
   }


}

WaveFile
*load_wav(const char* fname)
{
   FILE* input;
   int32_t chunkId1;
   int32_t fileSize;
   int32_t fileFormat;
   int32_t headerSize;
   int32_t chunkId3;
   int32_t dataSize;
   unsigned char buff;
   unsigned char fmt[3];

   WaveFile *wav;

   input = fopen(fname, "r");
   assert(input != NULL);

   wav = malloc(sizeof(WaveFile));
   assert(wav);

   fread(&chunkId1, 4, 1, input);
   fread(&fileSize, 4, 1, input);
   fread(&fileFormat, 4, 1, input);
   /* deal with junk blocks */
   do {
      fread(&buff, 1, 1, input);
   } while (buff != 'f');
   fread(&fmt, 1, 3, input);

   fread(&headerSize, 4, 1, input);
   fread(&wav->format, 2, 1, input);
   fread(&wav->channels, 2, 1, input);
   fread(&wav->sample_rate, 4, 1, input);
   fread(&wav->byte_rate, 4, 1, input);
   fread(&wav->bytes_per_sample, 2, 1, input);
   fread(&wav->bit_rate, 2, 1, input);
   fread(&chunkId3, 4, 1, input);
   fread(&dataSize, 4, 1, input);
   wav->data = (short*)malloc(sizeof(unsigned char) * dataSize);

   fread(wav->data, 1, dataSize, input);
   fclose(input);

   /*printf("cid1: %d\nfs: %d\nff: %d\n", chunkId1, fileSize, fileFormat);
   printf("cid2: %d\nhs: %d\ncid3: %d\nds: %d\n", chunkId2, headerSize,
          chunkId3, dataSize);
   printf("wavFormat %d wavChannels %d\n", *wavFormat, *wavChannels);
   printf("Format: %d", wav->format);
   printf("Channels: %d", wav->channels);
   printf("Sample Rate: %d", wav->sample_rate);
   printf("Byte Rate: %d", wav->byte_rate);*/

   wav->length = dataSize / (sizeof(short));

   return(wav);

}

#if 0
int
main(void)
{
  
   WaveFile* test = load_wav("../samples/kick.wav");
}
#endif

