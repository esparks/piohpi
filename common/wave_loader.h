/* Header file for PCM WAVE file reader */
#ifndef WAVE_LOADER_H
#define WAVE_LOADER_H
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>


/* Struct to hold data */

typedef struct {
   size_t  length;
   int32_t sample_rate;
   int32_t byte_rate;
   int16_t *data;
   int16_t channels;
   int16_t format;
   int16_t bytes_per_sample;
   int16_t bit_rate;
} WaveFile;

/* Function: Open file fname, read data into struct, return struct or NULL
 * if failed */


WaveFile *load_wav(const char* fname);
#endif

