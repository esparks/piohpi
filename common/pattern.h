/* pattern.h */
#ifndef PATTERN_H
#define PATTERN_H

#include <stdlib.h>
#include <stdint.h>

#define MAX_TRACKS 8
#define EVENTS_PER_TRACK 16

typedef struct {
   int trigger;
   int note_val;
} event_t;

typedef struct {
   event_t step[EVENTS_PER_TRACK];
   void *plugin_handle; /*i could probably get rid of this */
} track_t;


typedef struct {
   track_t tracks[MAX_TRACKS];
   int curr_step;
   int max_steps;
   int tempo;
   unsigned char mutes[MAX_TRACKS];
   int16_t id;
} pattern_t;


pattern_t* create_new_pattern();
track_t* get_track(int trackno);
event_t* get_step(pattern_t*, int trackno, int stepno);
void advance_pattern(pattern_t* pattern);

#endif
