#include "pop_utils.h"
#include <stdlib.h>
#include <time.h>

void
pop_nan_nap(long int nanoseconds)
{
   struct timespec req, rem;

   req.tv_nsec = nanoseconds;
   req.tv_sec = 0;

   rem.tv_nsec = 0;
   rem.tv_sec = 0;
   nanosleep(&req, &rem);  
   /* causing freeze when try to wait out remainders  
   while (rem.tv_nsec != 0) {
      req.tv_nsec = rem.tv_nsec;
      req.tv_sec = rem.tv_sec;
      nanosleep(&req, &rem);
   }*/
}
