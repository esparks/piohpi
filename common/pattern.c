/* pattern.c 
   created by: Evan Sparks
   modified:   13/09/2013

*/
#include "pattern.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

pattern_t*
create_new_pattern()
{
   static int16_t cId = 0;

   int i = 0;
   int s = 0;

   pattern_t* new_pattern;

   new_pattern = malloc(sizeof(pattern_t));
   new_pattern->curr_step = 0;
   new_pattern->max_steps = EVENTS_PER_TRACK;
   new_pattern->tempo = 120;
   new_pattern->id = cId;
   /*printf("Making a pattern of %d tracks..\n", MAX_TRACKS);*/
   /* initialise all the steps to 0/no event */
   for (i = 0; i < MAX_TRACKS; i++) {

      for (s = 0; s < EVENTS_PER_TRACK; s++) {
        new_pattern->tracks[i].step[s].trigger = 0;
         new_pattern->tracks[i].step[s].note_val = 0;
      }
   }
   cId++;
   return (new_pattern);
}

void
advance_pattern(pattern_t *pattern)
{
   pattern->curr_step++;
   if (pattern->curr_step >= EVENTS_PER_TRACK)
      pattern->curr_step = 0;
}
