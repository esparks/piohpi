!#/bin/bash
echo -n performance | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
sudo service ntp stop
sudo service triggerhappy stop
sudo service dbus stop
sudo killall console-kit-daemon
sudo killall polkitd
sudo mount -o remount,size=128M /dev/shm
echo -n "1-1.1:1.0" | sudo tee /sys/bus/usb/drivers/smsc95xx/unbind

