/* PiOhPi Main server */


#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "../common/common_pipe.h"
#include <pthread.h>
#include <dlfcn.h>
#include "../common/common_buffers.h"
#include <sys/time.h>
#include "../common/pattern.h"
#include "pop_server.h"
#include "pop_server_callbacks.h"
#include <string.h>
#include <assert.h>

#define SAMPLE_RATE 44100
#define BUFFER_SIZE 512
#define STEREO 2
#define START_ON_PLAY 0 
#define CLOCK_ON_SAMPLES 1
#define TIMER_TEST 0

static sequencer_data_t *data;
static int quit = 0;

void setup_test_tracks(pattern_t *pattern);
void update_loop(pattern_t *pattern, int nsamples);
void message_callback(unsigned char message_size, 
                      unsigned char* message_data);

/* one line func, replace inline in receiving function */
void 
message_callback(unsigned char message_size, unsigned char* message_data)
{
   handle_message(data, message_size, message_data);
}

buffer_type_t
*get_buffers(int nsamples)
{
   /* Need to ret buffers full of 0's when playing == 0 */
   int               i;
   buffer_type_t     *curr;
   
   /* set to time function */
#if TIMER_TEST
   static long int   num_calls = 0;
   static long int   running_tot = 0;
   
   struct timespec   time;
   long int          start_millis = 0;
   long int          end_millis = 0;
   long int          delta = 0;
   
   clock_gettime(CLOCK_MONOTONIC, &time);
   start_millis = (time.tv_sec * 1000) + ((float)time.tv_nsec / 1000.0);
   num_calls++;
#endif

   /*check for a pattern/step advance */
#if CLOCK_ON_SAMPLES == 1
   if(data->playing)
      update_loop(data->pattern, nsamples);
#endif

   /* create storage and get data from the plugins */
   curr = malloc(sizeof(buffer_type_t));
   curr->buffers = malloc(sizeof(int16_t**) * data->num_plugins);
   curr->num_buffers = data->num_plugins;
   curr->samples = nsamples;
   
   if(data->playing) {
      for (i = 0; i < data->num_plugins; i++) {
         plugin_t* plugin = &data->plugin[i];
         curr->buffers[i] = plugin->gen_buffer(plugin->instance, 
                                               nsamples);
      }
   } else {
      for (i = 0; i < data->num_plugins; i++) {
        curr->buffers[i] = calloc(nsamples * 2, sizeof(int16_t));
      }
   }

#if TIMER_TEST
   clock_gettime(CLOCK_MONOTONIC, &time);
   end_millis = (time.tv_sec * 1000) + ((float)time.tv_nsec / (float)1000);
   delta = end_millis - start_millis;
   running_tot += delta;
   fprintf(stdout, "num_calls: %li average ttc: %f curr ttc: %d\n", num_calls,
   (float)running_tot / (float)num_calls, delta);
#endif

   return (curr);
}


void*
run_pipe_output(void* args)
{
   struct timespec req, rem;
   int fitest;
   int active = 1;
   FILE *p_out;

   printf("openening output pipe"); 
   fitest = mkfifo(PIOH_PIPE_OUT, S_IWUSR | S_IRUSR | S_IRGRP);
   /* check errors would be done here */
   printf("fitest on out = %d\n", fitest);
  
   p_out = fopen(PIOH_PIPE_OUT, "wb");   
   printf("output file opened\n");
   while(quit == 0) 
   {
      int step;
      int32_t size = sizeof(pattern_t);
    
      /* write out the current pattern */
      if (data->pattern != NULL) {
         fwrite(&size, sizeof(int32_t), 1, p_out);
         fwrite(data->pattern, size, 1, p_out);
         fflush(p_out);
         data->changed = 0;
         /* go to sleep to prevent spinning */
         rem.tv_nsec = 1000;
         rem.tv_sec = 0;

         step = data->pattern->curr_step;

         while (data->pattern->curr_step == step && data->changed == 0) {
            req.tv_nsec = rem.tv_nsec;
            req.tv_sec = rem.tv_sec;
            nanosleep(&req, &rem);
         }
      }

      
   }
   
   return(NULL);

}

void*
run_pipe_input(void* args)
{
   int fitest;
   FILE *p_in;
   unsigned char message_size;
   unsigned char *message;
   
   
   fitest = mkfifo(PIOH_PIPE_IN, S_IWUSR | S_IRUSR | S_IRGRP);
   printf("fitest val is %d\n", fitest);
   
   p_in  = fopen(PIOH_PIPE_IN, "rb");
   assert(p_in != NULL);

   while(quit == 0) {
      fread(&message_size, sizeof(int32_t), 1, p_in);
      message = malloc(sizeof(unsigned char) * message_size);
      fread(message, message_size, 1, p_in);
      message_callback(message_size, message);
      free(message);
      data->changed = 1;
   }
   return(NULL);
}





int main(int argc, char** argv)
{
   pthread_t thread_pipe_out;
   pthread_t thread_pipe_in;
   
   int i, p_err;
   
   int (*initialise_mixer)(unsigned int, size_t, int, int,
                           buffer_type_t*(*get_buffer_func)(int)); 

   void (*start_audio)(void);
   void (*shutdown_mixer)(void);


   data = malloc(sizeof(sequencer_data_t));
   data->p_id = 0;
   data->last_time = 0;
   data->last_time_fetch = 0;
   data->changed = 0;
   data->num_plugins = MAX_TRACKS;
   data->plugin = malloc(sizeof(plugin_t) * data->num_plugins);
   printf("Starting POP SERVER\n\n\n");

   p_err = pthread_create(&thread_pipe_out, NULL, run_pipe_output, NULL);
   if (p_err != 0) {
      printf("Couldn't create output thread.\n");
      exit(EXIT_FAILURE);
   }

   p_err = pthread_create(&thread_pipe_in, NULL, run_pipe_input, NULL);
   if (p_err != 0) {
      printf("Couldn't create input thread.\n");
      exit(EXIT_FAILURE);
   }

   data->buffer_lock = malloc(sizeof(pthread_mutex_t));
   p_err = pthread_mutex_init(data->buffer_lock, NULL);

   
   /* load in the dynamic libraries! */
   printf("Loading plugins\n");

   for(i = 0; i < data->num_plugins; i++) {
      printf("Loading plugin %d\n", i);

      if (i < 6)
         data->plugin[i].handle = dlopen("../plugins/sample_player.so", RTLD_LAZY);
      else
         data->plugin[i].handle = dlopen("../plugins/pulse_synth.so", RTLD_LAZY);

      if (data->plugin[i].handle == NULL) {
         printf("Couldn't open plugin!");
         exit(EXIT_FAILURE);
      }

      *(void **)(&data->plugin[i].init) 
                  = dlsym(data->plugin[i].handle, "init_plugin");
      
      *(void **)(&data->plugin[i].gen_buffer) 
                  = dlsym(data->plugin[i].handle, "get_buffer");
      
      *(void **)(&data->plugin[i].send_payload) 
                  = dlsym(data->plugin[i].handle, "receive_payload");
      
      *(void **)(&data->plugin[i].set_master_volume) 
                  = dlsym(data->plugin[i].handle, "set_master_volume");
      
      *(void **)(&data->plugin[i].instance) 
                  = data->plugin[i].init();
   }
     
   printf("Loading mixer...\n");

   data->mixer = dlopen("./audio_mixer/alsa_mixer.so", RTLD_LAZY);
   if (data->mixer == NULL) {
      printf("Couldn't open mixer!");
      exit(EXIT_FAILURE);
   }


   /* make a new pattern */
   printf("Creating new patterns\n");
   data->pbank = malloc(sizeof(pattern_t*) * DEFAULT_PATTERN_NUM);
   
   for (i = 0; i < DEFAULT_PATTERN_NUM; i++)
      data->pbank[i] = create_new_pattern();
   
   data->pattern = data->pbank[0];
   setup_test_tracks(data->pattern);


   *(void**)&initialise_mixer = dlsym(data->mixer, "initialise_mixer");
   *(void**)&start_audio = dlsym(data->mixer, "start_audio_output");
   *(void**)&shutdown_mixer = dlsym(data->mixer, "do_quit");


   printf("initialising mixer...\n");
   initialise_mixer(SAMPLE_RATE, BUFFER_SIZE, 0, 0, &get_buffers);
   
   printf("Starting main loop\n");
   
   send_plugin_events(data);
   data->playing = START_ON_PLAY;
   while(!quit) {
   #if !CLOCK_ON_SAMPLES
      if (data->playing == 1) {
         data->last_time = 0;
         data->last_time_fetch = 0;
         data->pattern->curr_step = 0;
         send_plugin_events(data);
         while(data->playing && (!quit)) {
            update_loop(data->pattern, 0);
         }
      }
   #endif
   }
   shutdown_mixer();

   return (EXIT_SUCCESS);

}

void update_loop(pattern_t* pattern, int nsamples)
{
   int bpm = pattern->tempo;
   float steps_sec = (float)(bpm * 4) / 60.0;

#if CLOCK_ON_SAMPLES
   float samp_step = (float)SAMPLE_RATE / steps_sec;
   static int samp_count = 0;
   samp_count += nsamples;

  
   if (samp_count >= samp_step) {
      advance_pattern(data->pattern);
      send_plugin_events(data);
      samp_count -= samp_step;
   }
   data->last_read = 0;
   return;
#endif

#if !CLOCK_ON_SAMPLES
   double sec = 1000000000.0
   float ns_per_step = sec / steps_sec;
   static struct timespec time_now;
   struct timespec req, rem;
   time_t now, delta;

   clock_gettime(CLOCK_REALTIME, &time_now);
   now = time_now.tv_nsec;

   if (data->last_time == 0)
      data->last_time = now;

   if (data->last_time > now)
      now += sec;

   delta = now - data->last_time;
   
   if (delta < 0) delta *= -1;

   if (delta >= ns_per_step) {
      advance_pattern(data->pattern);
     /* deliver all the payloads */
      send_plugin_events(data);
      data->last_time = time_now.tv_nsec;
   } else {
      req.tv_nsec = 100;
      req.tv_sec = 0;
      nanosleep(&req, &rem);
      if (rem.tv_nsec > 0) {
         req.tv_nsec = rem.tv_nsec;
         req.tv_sec = 0;
         nanosleep(&req, &rem);
      }
   }
#endif
}

/*
   For testing without the GUI, set up a pattern.
*/

void
setup_test_tracks(pattern_t* pattern)
{
   int i = 0;
   int s = 0;
 
   pattern->tempo = 120;

   for (s = 0; s < EVENTS_PER_TRACK; s++) {
      pattern->tracks[i].step[s].note_val = 1;
      if (s == 0 || s == 4 || s == 8 || s == 12) {
         pattern->tracks[i].step[s].trigger = 1;
      }
   }

   i = 1;

   for (s = 0; s < EVENTS_PER_TRACK; s++) {
      if (s == 2 || s == 6 || s == 10 || s == 14) {
         pattern->tracks[i].step[s].trigger = 1;
         pattern->tracks[i].step[s].note_val = 2;
      } else {
         pattern->tracks[i].step[s].trigger = 0;
         pattern->tracks[i].step[s].note_val = 2;
      }      
   }

   i = 2;
   for (s = 0; s <EVENTS_PER_TRACK; s++) {
      pattern->tracks[i].step[s].trigger = 1;
      pattern->tracks[i].step[s].note_val = 3;
   }

   i = 3;
   data->plugin[i].set_master_volume(data->plugin[i].instance, 0.3f);
   for (s = 0; s < EVENTS_PER_TRACK; s++) {
      if (s == 4 || s == 12) {
         pattern->tracks[i].step[s].trigger  = 1;
         pattern->tracks[i].step[s].note_val = 4;
      } else {
         pattern->tracks[i].step[s].note_val = 4;
      }

   }

   i = 4;
   data->plugin[i].set_master_volume(data->plugin[i].instance, 0.4f);
   pattern->tracks[i].step[15].trigger  = 5;
   for (s = 0; s < EVENTS_PER_TRACK; s++)
      pattern->tracks[i].step[s].note_val = 5;

/* If going to try 10 tracks these will set up for you.
   i = 8;
   data->plugin[i].set_master_volume(data->plugin[i].instance, 0.3f);
   pattern->tracks[i].step[2].trigger  = 1;
   pattern->tracks[i].step[6].trigger  = 1;
   pattern->tracks[i].step[10].trigger  = 1;
   pattern->tracks[i].step[14].trigger  = 1;
   pattern->tracks[i].step[15].trigger  = 1;
   for (s = 0; s < EVENTS_PER_TRACK; s++)
      pattern->tracks[i].step[s].note_val = 6;

 
   i = 9;
   data->plugin[i].set_master_volume(data->plugin[i].instance, 0.5f);
   pattern->tracks[i].step[13].trigger = 1;
   pattern->tracks[i].step[15].trigger = 1;
   for (s = 0; s < EVENTS_PER_TRACK; s++)
      pattern->tracks[i].step[s].note_val = 7;
*/

   i = 5;
   data->plugin[i].set_master_volume(data->plugin[i].instance, 0.2f);
   for (s = 0; s < EVENTS_PER_TRACK; s++) {
      if (s % 2 == 0) {
         pattern->tracks[i].step[s].trigger = 1;
      }
      pattern->tracks[i].step[s].note_val = 8;
   }

   i = 6;
   data->plugin[i].set_master_volume(data->plugin[i].instance, 0.05f); 
   data->plugin[i].set_master_volume(data->plugin[i].instance, 0.1f);
   for(s = 0; s < EVENTS_PER_TRACK; s++)
   {
      int interval = 0;
      int v = s % 3;

      if (v == 1)
         interval = 4;
      else if (v == 2)
         interval = 7;
      
      pattern->tracks[i].step[s].note_val = 52 + interval;


      pattern->tracks[i + 1].step[s].note_val = s + 24;
   }

   printf("test tracks created\n");
  
}


void
do_quit()
{
	quit = 1;
}
