#include <stdlib.h>
#include <stdio.h>
#include "pop_server_callbacks.h"
#include "pop_server.h"
#include "../common/pattern.h"
#include "../common/common_pipe.h"
#include <assert.h>

void
change_tempo(sequencer_data_t *seq, int change)
{
   seq->pattern->tempo += change;
}

/* sends all relevant data to the sound generators on step update */
void
send_plugin_events(sequencer_data_t* seq)
{
   int i;
   for (i = 0; i < seq->num_plugins; i++) {
      if (seq->pattern->mutes[i] == 0) {
         seq->plugin[i].send_payload(seq->plugin[i].instance, 
                             &seq->pattern->tracks[i].step[seq->pattern->curr_step]); 
      }
   }
}

/* clears a pattern's triggers. note values etc. remain */
void
clear_triggers(sequencer_data_t *seq)
{
   int i, j;
   pattern_t *pattern = seq->pattern;
   for (i = 0; i < MAX_TRACKS; i++) {
      for (j = 0; j < EVENTS_PER_TRACK; j++) {
         pattern->tracks[i].step[j].trigger = 0;
      }
   }
}

int
normalize_p_id(int p_id)
{
   if (p_id >= DEFAULT_PATTERN_NUM)
      p_id = 0;

   if (p_id < 0)
      p_id = DEFAULT_PATTERN_NUM - 1;
   
   return (p_id);
}

void
increment_track_notes(sequencer_data_t *seq, int track, int change)
{
   int i;
   pattern_t *pattern = seq->pattern;
   for (i = 0; i < EVENTS_PER_TRACK; i++) {
      pattern->tracks[track].step[i].note_val += change;
      if (pattern->tracks[track].step[i].note_val < 0)
         pattern->tracks[track].step[i].note_val = 0;
      if (pattern->tracks[track].step[i].note_val > 127)
         pattern->tracks[track].step[i].note_val = 127;
   }

}

/* Process messages incoming from the client */
void
handle_message(sequencer_data_t *seq, unsigned char size, unsigned char *message)
{

   /* set the quit flag */
   if (message[0] == POP_EXIT_PROGRAM) {
      do_quit();
      return;
   }

   /* Starts (and stops if playing) playback */
   if (message[0] == POP_START_PLAYBACK) {
      seq->playing += 1;
      seq->playing = seq->playing % 2;
      seq->pattern->curr_step = 0;
      send_plugin_events(seq);
      return;
   }

   if (message[0] == POP_STOP_PLAYBACK) {
      seq->playing = 0;
      return;
   }

   /* mute or unmute a given track */
   if (message[0] == POP_MUTE_CHANNEL_1) {
      seq->pattern->mutes[message[1]] += 1;
      seq->pattern->mutes[message[1]] = seq->pattern->mutes[message[1]] % 2;
      return;
   }

   /* activate or deactivate trigger */
   if (message[0] == POP_FLIP_STEP_TRIGGER) {
      int track = message[1];
      int step = message[2];
      seq->pattern->tracks[track].step[step].trigger += 1;
      seq->pattern->tracks[track].step[step].trigger =
                    seq->pattern->tracks[track].step[step].trigger % 2;
      return;
   }

   /* reset the step pointer to 0 */
   if (message[0] == POP_RESET_PLAYBACK) {
      seq->last_time = 0;
      seq->last_time_fetch = 0;
      seq->pattern->curr_step = 0;
      send_plugin_events(seq);
      return;
   }

   /* clear the curent pattern */
   if (message[0] == POP_CLEAR_ALL_TRIGGERS) {
      clear_triggers(seq);
      return;
   }

   if (message[0] == POP_INC_STEP_NOTE) {
      int track = message[1];
      int step = message[2];
      event_t* ev = &seq->pattern->tracks[track].step[step];
      ev->note_val++;

      /*** PUT THIS IN A DEFINE: In pattern.h maybe ***/
      if (ev->note_val > 127) ev->note_val = 127;
      return;
   }

   if (message[0] == POP_DEC_STEP_NOTE) {
      int track = message[1];
      int step = message[2];
      event_t *ev = &seq->pattern->tracks[track].step[step];
      ev->note_val--;
      if (ev->note_val < 0) ev->note_val = 0;
      return;
   }

   if (message[0] == POP_CHANGE_TEMPO) {
      int change = (signed char)message[1];
      change_tempo(seq, change);
      return;
   }

   /* change to the next pattern */
   if (message[0] == POP_PATTERN_UP) {
      /* switch to new pattern matching the current step 
       * alternate behaviour could be to set a flag and switch
       * on the pattern loop */
      int c_step = seq->pattern->curr_step;
      seq->p_id++;
      seq->p_id = normalize_p_id(seq->p_id);
      seq->pattern = seq->pbank[seq->p_id];
      seq->pattern->curr_step = c_step;
      return;
   }

   /* change to the previous pattern */
   if (message[0] == POP_PATTERN_DOWN) {
      int c_step = seq->pattern->curr_step;
      seq->p_id--;
      seq->p_id = normalize_p_id(seq->p_id);
      seq->pattern = seq->pbank[seq->p_id];
      seq->pattern->curr_step = c_step;
      return;
   }

   /* increase note value across the row */
   if (message[0] == POP_INC_TRACK_NOTE) {
      int track = message[1];
      increment_track_notes(seq, track, 1);
      return;
   }

   /* decrease note value across the row */
   if (message[0] == POP_DEC_TRACK_NOTE) {
      int track = message[1];
      increment_track_notes(seq, track, -1);
      return;
   }

   /* increase note value across the row by 12 */
   if (message[0] == POP_INC_TRACK_OCTAVE) {
      int track = message[1];
      increment_track_notes(seq, track, 12);
      return;
   }

   /* decrease the note value across the row by 12 */
   if (message[0] == POP_DEC_TRACK_OCTAVE) {
      int track = message[1];
      increment_track_notes(seq, track, -12);
      return;
   }
}


