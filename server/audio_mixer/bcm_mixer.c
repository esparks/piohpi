/* Mixes n buffers into single stereo output buffer and sends it to the pi
   audio output */

/* Uses OpenMAX IL */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <semaphore.h>

#include "bcm_host.h"
#include "ilclient.h"


/* Audio device data */
typedef struct {
   sem_t sema;
   ILCLIENT_T *client;
   COMPONENT_T *audio_render;
   COMPONENT_T *list[2];
   OMX_BUFFERHEADERTYPE *user_buffer_list;
   uint32_t num_buffers;
   uint32_t bytes_per_sample;
} AUDIODEVICE_STATE_T

static void input_buffer_callback(void *data, COMPONENT_T *comp)
{
   /* Called if the device has an empty buffer */
}

/* Set up the audio renderer with out desired settings.
 * Perform error checking, returns -1 in case of failure.
 *
 */
void create_audio_device(AUDIODEVICE_STATE_T **handle,
                         uint32_t sample_rate,
                         uint32_t num_channels,
                         uint32_t bit_depth,
                         uint32_t num_buffers,
                         uint32_t buffer_size);
{
   uint32_t bytes_per_sample = (bit_depth * num_channels) >> 3;
   int32_t ret = -1;

   *handle = NULL;

   /* Check parameters for validity */
   if (sample_rate >= 8000 && sample_rate <= 96000 &&
         (num_channels == 1 || num_channels == 2 || num_channels == 4 || 
          num_channels == 8) &&
          num_buffers > 0 && buffer_size >= bytes_per_sample)
   {
      /* buffer lengths must be 16 byte aligned */
      int size = (buffer_size + 15) & ~15;
      AUDIODEVICE_STATE_T *st;
      st = calloc(1, sizeof(AUDIODEVICE_STATE_T));
      
      if(st)
      {
         OMX_ERRORTYPE error;
         OMX_PARAM_PORTDEFINITIONTYPE param;
         OMD_AUDIO_PARAM_PCMMODETYPE pcm;
         int32_t sem;
         ret = 0;
         *handle = st;

         /* initialise the semaphore */
         s = sem_init(&st->sema, 0, 1);
         assert(s == 0);

         /* fill out the device data */
         st->bytes_per_sample = bytes_per_sample;
         st->num_buffers = num_buffers;
         st->client = iclient_init();
         
         iclient_set_empty_buffer_done_callback(st-client,
            input_buffer_callback, st);

         /* check out device has opened succesfully */
         assert(st->audio_render != NULL);

         st->list[0] = st->audio_render;

         /* set up the buffers */
         memset(&param, 0, sizeof(OMX_PARAM_PORTDEFINITIONTYPE));
         param.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
         param.nVersion.nVersion = OMX_VERSION;
         param.nPortIndex = 100;

         error = OMX_GetParameter(ILC_GET_HANDLE(st->audio_render), 
                                  OMX_IndexParamPortDefinition,
                                  &param);

         assert (error == OMX_ErrorNone);

         param.nBufferSize = size;
         param.nBufferCountActual = num_buffers;

         error = OMX_SetParameter(ILC_GET_HANDLE(st->audio_render),
                                  OMX_IndexParamPortDefinition, &param);
         
         assert(error == OMX_ErrorNone);

         /* set up PCM output */
         memset(&pcm, 0, sizeof(OMX_AUDIO_PARAM_PCMMODETYPE));
         pcm.nSize = sizeof(OMD_AUDIO_PARAM_PCMMODETYPE);
         pcm.nVersion.nVersion = OMX_VERSION;
         pcm.nPortIndex = 100;
         pcm.nChannels = num_channels;
         pcm.eNumData = OMX_NumericalDataSigned;
         pcm.eEndian = OMX_EndianLittle;
         pcm.nSamplingRate = sample_rate;
         pcm.bInterleaved = OMX_TRUE;
         pcm.nBitPerSample = bit_depth;
         pcm.ePCMMode = OMX_AUDIO_PCMModeLinear;

         /* map the channels */

         switch(num_channels) {
         case 1:
            pcm.eChannelMapping[0] = OMX_AUDIO_ChannelCF;
            break;
         case 8:
            pcm.eChannelMapping[0] = OMX_AUDIO_ChannelLF;
            pcm.eChannelMapping[1] = OMX_AUDIO_ChannelRF;
            pcm.eChannelMapping[2] = OMX_AUDIO_ChannelCF;
            pcm.eChannelMapping[3] = OMX_AUDIO_ChannelLFE;
            pcm.eChannelMapping[4] = OMX_AUDIO_ChannelLR;
            pcm.eChannelMapping[5] = OMX_AUDIO_ChannelRR;
            pcm.eChannelMapping[6] = OMX_AUDIO_ChannelLS;
            pcm.eChannelMapping[7] = OMX_AUDIO_ChannelRS;
            break;
         case 4:
            pcm.eChannelMapping[0] = OMX_AUDIO_ChannelLF;
            pcm.eChannelMapping[1] = OMX_AUDIO_ChannelRF;
            pcm.eChannelMapping[2] = OMX_AUDIO_ChannelLR;
            pcm.eChannelMapping[3] = OMX_AUDIO_ChannelRR;
            break;
         case 2:
            pcm.eChannelMapping[0] = OMX_AUDIO_ChannelLF;
            pcm.eChannelMapping[1] = OMX_AUDIO_ChannelRF;
            break;
         }

         error = OMX_SetParameter(ILC_GET_HANDLE(st->audio_render),
                                  OMX_IndexParamAudioPcm, &pcm); 
         assert (error == OMX_ErrorNone);

         ilclient_change_component_state(st->audio_render, OMX_StateIdle);
         
         if (ilclient_enable_port_buffers(st->audio_render, 100, NULL, NULL,
               NULL) < 0)
         {
            /* error has occurred during setup */
            ilclient_change_component_state(st->audio_render, 
                                            OMX_StateLoaded);

            ilclient_cleanup_components(st->list);

            error = OMX_Deinit();
            assert (error == OMX_ErrorNone);

            ilclient_destroy(st->client);
            sem_destroy(&st->sema);
            free(st);
            *handle = NULL;
            return -1;
         }


         ilclient_change_component_state(st->audio_render,
                                         OMD_StateExecuting);

      }
   }

   return ret;
}
                           


void init_audio_output()
{

}








void mix_to_output(size_t buff_size, int* buffer)
{




}
