/* tests the mixer */


// really just clipped code dump

void generate_square_wave(short* buffer, int size);



void
generate_square_wave(short* buffer, int size)
{
   /* fill buffer with some stuff and nonsense */
   static int widmodrate = 440;
   static int widmodcount = 0;
   static int dir = 1;

   int flip = 1;
   int i = 0;
   int count = 0;

   short *ibuf = buffer;
   short pcm_max = 32767 / 16;
   short pcm_min = -32767 / 16;
   //short pcm_max = 0;
   //short pcm_min = 0;

   int reset_wave = 3;   
   int reset_count = 0;




   for (i = 0; i < (size / 2); i++)
   {
   /*
      reset_count++;
      if (reset_count >= reset_wave) {
         reset_count = 0;
         count = 0;
         flip = 1;
      }*/
   
      if (count >= pwidth) {
         count = 0;
         flip *= -1;
      }
      /*interleaved don't forget! */

      // write left channel
      if (flip == 1)
         *ibuf = pcm_max;
      else
         *ibuf = pcm_min;

      ibuf++;
      // write right channel
      if (flip == 1)
         *ibuf = pcm_max;
      else
         *ibuf = pcm_min;

      ibuf++;
      count++;

      
      /* increment the pulse_width to pitch the sound */
   
      widmodcount++;
      if (widmodcount > widmodrate) {
         widmodcount = 0;
         pwidth += dir;
      }

      if (pwidth >= (size / 4) || pwidth <= 0) {
         dir *= -1;
      }
   }

}



