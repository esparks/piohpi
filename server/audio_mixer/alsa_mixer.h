#ifndef ALSA_MIXER_H
#define ALSA_MIXER_H

#include <stdlib.h>
#include <alsa/asoundlib.h>
#include "../../common/common_buffers.h"

int initialise_mixer(unsigned int sample_rate,
                     size_t buffer_size,
                     int dev_num,
                     int chan_num,
                     buffer_type_t* (*get_buffers_function)(int));

int start_audio_output();
int stop_audio_output();
void do_quit();

#endif

