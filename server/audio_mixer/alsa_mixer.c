#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <alsa/asoundlib.h>
#include <poll.h>
#include "../../common/common_buffers.h"
#include "alsa_mixer.h"
#include <string.h>

#define HEADROOM 0.5f

static snd_pcm_t *device;
static size_t g_buffer_size;
static buffer_type_t* (*get_pcm_buffers)(int nsamples);
static int quit = 0;

/* testing hack, max and min.. */
static int comp_thresh = 18000; /*32767 - ((int16_t) 32767 / 2);*/  
static float comp_release = 0.0001;
static float comp_attack = 0.0005;
static int comp_trigger = 0;
static float comp_reduce = 0.7;
static float comp_gain = 1.0;
static int comp_edge = 1;

void clear_mmap_areas(const snd_pcm_channel_area_t *areas, 
                           snd_pcm_sframes_t avail, 
                           snd_pcm_uframes_t offset);

void alsa_callback(snd_async_handler_t *pcm_callback);

int16_t sum_channels(int16_t** channels, float gain, 
                  int num_channels, int pos)
{
   int j;
   int32_t temp;

   temp = 0;
  
   for (j = 0; j < num_channels; j++) { 
      temp += (float)(channels[j][pos]) * HEADROOM;
   }
   /* Removed the division .. for accuracy's sake
    * mixing is a simple sum of channels 
    * It is up to user to adjust track gain so as not to clip */

   /* we do multiply by a headroom amount to find a balance 
    * between having enough headroom and the pi audio 
    * signal-noise ratio. */
   /*temp = (float)temp * HEADROOM;*/
   if (abs(temp) > comp_thresh) {
      /*printf("comp at edge %d\n", comp_edge);*/
      comp_edge = 0;
   }
   if (comp_edge <= 0) {
      if (comp_gain > comp_reduce) {
         /*printf("falling gain = %f comp_reduce %f \n", comp_gain, comp_reduce);*/
         comp_gain -= comp_attack;
      } else {
         /*printf("hit bottom, rising\n");*/
         comp_edge = 1;
      }  
   } else {
      if (comp_gain < 1.0) {
         comp_gain += comp_release;
      }
      if (comp_gain > 1.0) {
         comp_gain == 1.0;
      } 
   }

   return (int16_t)((float)temp * comp_gain);
}


int
initialise_mixer(unsigned int sample_rate, size_t buffer_size, 
                 int dev_num, int chan_num, 
                 buffer_type_t* (*get_buffers_function)(int))
{
   int ret = 0, err;
   int dir = -1;
   unsigned int buffer_us = 50000;
   /*unsigned int period_us = 1000;*/

   snd_pcm_hw_params_t *hw;
   snd_pcm_sw_params_t *sw;
   snd_pcm_sframes_t avail = 0;
   snd_pcm_uframes_t offset;
   snd_pcm_uframes_t frames;
   const snd_pcm_channel_area_t *areas;
 
   char device_name[16]; 
   snd_async_handler_t *pcm_callback;

   //sprintf(device_name,"hw:%d,%d", dev_num, chan_num);
   sprintf(device_name,"plughw:%d,%d", 1, 0);
   g_buffer_size = buffer_size;
   get_pcm_buffers = get_buffers_function;
   printf("opening device %s\n", device_name);
   /* initialise the device */
   err = snd_pcm_open(&device, device_name, SND_PCM_STREAM_PLAYBACK, 0);

   if (err < 0)
      printf("Couldn't open device\n");

   err = snd_pcm_hw_params_malloc(&hw);
   if (err < 0)
      printf("Couldn't allocate ram for device\n");

   err = snd_pcm_hw_params_any(device, hw);
   if (err < 0)
      printf("Couldn't get hw params\n");

   /* Use memory mapping to access data continously
    * w/ no copy time */

   err = snd_pcm_hw_params_set_access(device, hw,
                                      SND_PCM_ACCESS_MMAP_INTERLEAVED);
   if (err < 0)
      printf("couldn't set hw access\n");

   err = snd_pcm_hw_params_set_format(device, hw,
                                      SND_PCM_FORMAT_S16_LE);
   if (err < 0)
      printf("couldn't set format\n");

   if (err < 0)
      printf("Coudln't set format to 16bit stereo\n");

   err = snd_pcm_hw_params_set_rate_near(device, hw, &sample_rate, 0);

   printf("Sample rate requested:%d\n", sample_rate);
   printf("Actual rate:%d\n", sample_rate);
   
   err = snd_pcm_hw_params_set_buffer_time_near(device, hw, &buffer_us, &dir);
   
   //if (err < 0)
   //   printf("error setting buffer time\n");

   /* causing segfault snd_pcm_hw_params_set_period_size_near(device, hw, 2048,
    * dir);
    */

   //err = snd_pcm_hw_params_set_period_time(device, hw, buffer_size / 2, dir);
   //if (err < 0)
     // printf("error setting period time\n");

   err = snd_pcm_hw_params_set_channels(device, hw, 2);
   if (err < 0)
      printf("couldn't set channels\n");

   snd_pcm_hw_params(device, hw);

   snd_pcm_hw_params_free(hw);

   err = snd_pcm_sw_params_malloc(&sw);

   err = snd_pcm_sw_params_current(device, sw);

   err = snd_pcm_sw_params_set_avail_min(device, sw, buffer_size);
   if (err < 0)
      printf("Couldn't set avail min\n");

   err = snd_pcm_sw_params_set_start_threshold(device, sw, 0U);

   err = snd_pcm_sw_params(device, sw);

   snd_pcm_prepare(device);


   /* clear any garbage data in the ring buffer */
   printf("getting mmap data\n");
   printf("Checking available update shtuff...\n");
   
   avail = snd_pcm_avail_update(device);
   frames = g_buffer_size;

   while (avail > 0) {
      snd_pcm_mmap_begin(device, &areas, &offset, &frames);
      clear_mmap_areas(areas, frames, offset);  
      avail = snd_pcm_avail_update(device);
      snd_pcm_mmap_commit(device, offset, frames);
   }
   snd_async_add_pcm_handler(&pcm_callback, device, alsa_callback, NULL);

   err = snd_pcm_start(device);
   printf("start code is %d\n", err);
   if (err < 0) {
      printf("couldn't start audio output!!!\n");
   }

   return(ret);    
}

void 
clear_mmap_areas(const snd_pcm_channel_area_t *areas, 
                           snd_pcm_sframes_t avail, 
                           snd_pcm_uframes_t offset)
{
   int16_t *left = areas[0].addr;
   int16_t *right= areas[1].addr;
   int i = 0;
   right += (areas[1].step / 16);

   left += offset * (areas[0].step / 16);
   right += offset * (areas[1].step / 16);

   for (i = 0; i < avail; i++) {
      
      *left = (int16_t)0;
      *right = (int16_t)0;
      
      left += 2;
      right += 2;

   }
}


void 
fill_mmap_areas(const snd_pcm_channel_area_t *areas, 
                           snd_pcm_sframes_t avail, 
                           snd_pcm_uframes_t offset,
                           buffer_type_t* buffer_s)
{ 

   int16_t *left = areas[0].addr;
   int16_t *right= areas[1].addr;// + (areas[1].step / 16);
 
   int i, j;
   int16_t** buffers = malloc(sizeof(int16_t*) * buffer_s->num_buffers);
   unsigned char num_buffers = buffer_s->num_buffers;
    

   left += offset * (areas[0].step / 16);
   right += 1;
   right += offset * (areas[1].step / 16);

   /* need to copy pointer destinations 
    * so we can safely free later.
    */

   memcpy(buffers, buffer_s->buffers, sizeof(int16_t*) * buffer_s->num_buffers);

   /* sum all the buffers, increment the pointers one sample, repeat */ 
   for (i = 0; i < avail; i++) {
      
      *left = sum_channels(buffers, 1.0f, num_buffers, 0);
      for (j = 0; j < num_buffers; j++)
         buffers[j]++;

      *right = sum_channels(buffers, 1.0f, num_buffers, 0);
      for (j = 0; j < num_buffers; j++)
         buffers[j]++;

      left += 2;
      right += 2;

   }
   
   /* free pointer list, actual data will be freed
      outside */
   free(buffers);

}



void 
alsa_callback(snd_async_handler_t *pcm_callback)
{
   snd_pcm_t *pcm_handle = snd_async_handler_get_pcm(pcm_callback);
   snd_pcm_sframes_t avail;
   int err;
   const snd_pcm_channel_area_t *areas;
   snd_pcm_uframes_t offset;
   snd_pcm_uframes_t frames;
   snd_pcm_state_t state;
   int i;
   buffer_type_t *buffers;

   if (quit)
      exit(EXIT_SUCCESS);	

   avail = snd_pcm_avail_update(pcm_handle);
   frames = g_buffer_size;

   if (avail > 0) {      
    
      snd_pcm_mmap_begin(device, &areas, &offset, &frames);
      buffers = get_pcm_buffers(frames);

      /* ignore if empty buffer returned */
      if (buffers == NULL)
         return;

      fill_mmap_areas(areas, frames, offset, buffers);  
      snd_pcm_mmap_commit(device, offset, frames); 
   
      for (i = 0; i < buffers->num_buffers; i++) {
         free(buffers->buffers[i]);
      }
      free(buffers->buffers);
      free(buffers);
      avail = snd_pcm_avail_update(pcm_handle);
   }

    state = snd_pcm_state(device);

   if (state==SND_PCM_STATE_XRUN) {
      printf("xrun!");
      snd_pcm_prepare(device);
   } else {
      if(state == SND_PCM_STATE_PAUSED)
         printf("state paused\n");

      if(state == SND_PCM_STATE_OPEN)
         printf("state open\n");

      if(state == SND_PCM_STATE_SUSPENDED)
         printf("state suspended\n");

      if(state == SND_PCM_STATE_SETUP)
         printf("state setup\n");
      
      if(state == SND_PCM_STATE_DISCONNECTED)
         printf("state disconnected\n");

      if (state == SND_PCM_STATE_PREPARED){
         printf("state prepared\n");
         err = snd_pcm_start(device);
         printf("tried to start, got code %d", err);
      }

   }
}

int
start_audio_output()
{
   snd_pcm_start(device);
   return(EXIT_SUCCESS);
}

int stop_audio_output()
{
   printf("empty function: stop audio output\n");
   return(EXIT_FAILURE);
}

void
do_quit()
{
  quit = 1;
}

#if 0
int
main(void)
{
   int err;
   snd_pcm_sframes_t nsamples;
   printf("Testing init\n");

   wav = load_wav("../../samples/kick.wav");
   data = wav->data;
   initialise_mixer();
   snd_pcm_state_t state;
   snd_pcm_start(device);                   

   while(1) {

     state = snd_pcm_state(device);
      if (state==SND_PCM_STATE_XRUN) {
         printf("xrun!");
         snd_pcm_prepare(device);
      }

   }

}
#endif
