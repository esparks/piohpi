#include <sys/soundcard.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>


#include <errno.h>

#define MIDI_DEVICE "/dev/snd/seq"

int 
main(void)
{
   unsigned char input[4];

   int fMid;
   snd_ctl_t *device;
   snd_seq_t *mdev;
   snd_seq_event_t *ev;
   struct pollfd* pfd;
   int npfd = 0;


   //fMid = snd_ctl_open(&device, MIDI_DEVICE, SND_CTL_ASYNC);

   fMid = snd_seq_open(&mdev, "default", SND_SEQ_OPEN_DUPLEX, 0);


   if (fMid < 0) {
      printf("Couldn't open midi device %s\n", MIDI_DEVICE);
      return(EXIT_FAILURE);
   }


   printf("opened device succesfully. fMid returned %d\n", fMid);

   snd_seq_set_client_name(mdev, "Input");


   printf("attempting to create port...\n");

   int port_id;
   int i = 0;
   
   port_id = snd_seq_create_simple_port(mdev, "Input",
                                    SND_SEQ_PORT_CAP_WRITE |
                                    SND_SEQ_PORT_CAP_SUBS_WRITE,
                                    SND_SEQ_PORT_TYPE_APPLICATION);


   
      if (port_id < 0) {
         printf("Failed to create a port for input\n");
      } else {
         printf("opened port %d for input\n", port_id);
      }
   

   /*subscribin'*/
   snd_seq_addr_t sender, dest;
   snd_seq_port_subscribe_t *subs;
   sender.client = 20;
   sender.port = 0;
   dest.client = 128;
   dest.port = 0;

   snd_seq_port_subscribe_alloca(&subs);
   snd_seq_port_subscribe_set_sender(subs, &sender);
   snd_seq_port_subscribe_set_dest(subs, &dest);
   snd_seq_port_subscribe_set_queue(subs, 1);
   snd_seq_port_subscribe_set_time_update(subs, 1);
   snd_seq_port_subscribe_set_time_real(subs, 1);
   snd_seq_subscribe_port(mdev, subs);



   npfd = snd_seq_poll_descriptors_count(mdev, POLLIN);
   printf("num. of poll descriptors: %d\n", npfd);

   pfd = (struct pollfd*)alloca(npfd * sizeof(struct pollfd));
   
   snd_seq_poll_descriptors(mdev, pfd, npfd, POLLIN);
   printf("Entering loop\n");
   while(1) {
      int p = poll(pfd, npfd, 1000);
      if (p > 0) {
         snd_seq_event_input(mdev, &ev);
         printf("recieved event type: %d\n", ev->type);
      } else {
         printf("p is %d", p);
      }


   }


/*
   while(1) {
      read(fMid, &input, sizeof(input));

      if (input[0] == SEQ_MIDIPUTC) {
         printf("Received data: %d\n", input[1]);
      }

   }
*/


   close(fMid);



   return (EXIT_SUCCESS);

}
