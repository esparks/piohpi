#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include "../common/common_pipe.h"


void
choice_callback(int choice, FILE* p_out)
{  
   unsigned char message_size = 0;
   unsigned char* message_data;

   switch(choice) {
      case 1:
         message_size = 1;
         message_data = malloc(sizeof(unsigned char) * message_size);
         message_data[0] = POP_START_PLAYBACK;
         break;
      case 2:
         message_size = 1;
         message_data = malloc(sizeof(unsigned char) * message_size);
         message_data[0] = POP_STOP_PLAYBACK;
         break;
      case 3:
         message_size = 2;
         message_data = malloc(sizeof(unsigned char) * message_size);
         message_data[0] = POP_MUTE_CHANNEL_1;
         message_data[1] = 0;
         break;
      case 4:
         message_size = 2;
         message_data = malloc(sizeof(unsigned char) * message_size);
         message_data[0] = POP_MUTE_CHANNEL_1;
         message_data[1] = 7;
         break;
      default:
         break;
   }

   if (message_size != 0) {
      fwrite(&message_size, 1, 1, p_out);
      fwrite(message_data, 1, message_size, p_out);
      fflush(p_out);
   }

   free(message_data);

}

int main(void)
{

   /*FILE* p_in;*/
   FILE* p_out;
   int choice = 0;
   
   /*p_in = fopen(PIOH_PIPE_OUT, "rb");*/
   p_out = fopen(PIOH_PIPE_IN, "wb");
   
   printf("p0p server test driver\nSelect an option from the menu\n");
   printf("1. Start playback\n");
   printf("2. Stop playback\n");
   printf("3. Mute/Unmute channel 1\n");
   printf("4. Reset step to 0\n");
   printf("5. Quit\n");
   
   
   while(1) {
      scanf("%d", &choice);
      choice_callback(choice, p_out);
      if (choice == 5)
         break;
   }

   fclose(p_out);
   /*fclose(p_in);*/
   exit(EXIT_SUCCESS);
}
