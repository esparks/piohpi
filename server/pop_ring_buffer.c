
/* We cannot reliably use a mutex here because the ALSA interrupt 
   blocks any other threads from executing */

void
fill_buffer(int nsamples)
{	
   static int init = 0;
	int16_t *temp, *t;
	buffer_type_t *rb = &data->audio_buffer.set;
	ring_buffer_t *ab = &data->audio_buffer;

	int i;
   
   if (nsamples == 0)
      return;

	if (!init) {
		rb->buffers = malloc(sizeof(int16_t**) * data->num_plugins);
		data->audio_buffer.size = BUFFER_SIZE * STEREO;
		data->audio_buffer.write_offset = 0;
		data->audio_buffer.read_offset = 0;
		rb->num_buffers = data->num_plugins;
      data->audio_buffer.avail = 0;
		
		for (i = 0; i < data->num_plugins; i++) {
				rb->buffers[i] = calloc(BUFFER_SIZE * STEREO, sizeof(int16_t));
			}
	}
	
	
		
	/* updatebuffers*/
		for (i = 0; i < data->num_plugins; i++) {
			plugin_t* plugin = &data->plugin[i];

	      temp = calloc(nsamples * STEREO, sizeof(int16_t));
       
         t = temp;

         /* test */
         free(t);
         return;

			/* write into the ring buffer, over two writes if required */
			if (data->audio_buffer.write_offset + (nsamples * STEREO) >= data->audio_buffer.size) {
				/* split in two */
				int overflow = data->audio_buffer.write_offset + (nsamples * STEREO) - data->audio_buffer.size;
				int chunk1 = (nsamples * STEREO) - overflow;
				int16_t *curr = rb->buffers[i];
				curr += ab->write_offset;
				/* write chunk 1 */
				memcpy(curr, temp, sizeof(int16_t) * chunk1);
	
				curr = rb->buffers[i];
				temp += chunk1;
				memcpy(curr, temp, sizeof(int16_t) * overflow);;
			} else {
				/* single write */
				int16_t *curr = rb->buffers[i];
				curr += ab->write_offset;
				memcpy(curr, temp, sizeof(int16_t) * nsamples * STEREO);
			}
         assert(t);
         free(t);
		}
	
	 
	
	/* advance write head by samples written */
	ab->write_offset += nsamples * STEREO;
	if (ab->write_offset > ab->size)
		ab->write_offset -= ab->size;

   ab->avail += nsamples;

}

buffer_type_t
*fill_from_ring(int nsamples)
{
   static int num_calls = 0;

   /* ALSA fires callbacks indescriminately. We need to prevent later callbacks 
      from attempting to read before earlier ones have finished */

   while(num_calls != 0);

   num_calls++;
	buffer_type_t *curr = malloc(sizeof(buffer_type_t));
	int i;
	data->last_read = nsamples;
	ring_buffer_t *ab = &data->audio_buffer;
	curr->buffers = malloc(sizeof(int16_t**) * data->num_plugins);
	curr->num_buffers = data->num_plugins;
   curr->samples = nsamples;
   buffer_type_t *pregen = &ab->set;
   
	for (i = 0; i < data->num_plugins; i++) {

		curr->buffers[i] = malloc(sizeof(int16_t) * nsamples * STEREO);
#if 1 
      if (ab->read_offset + (nsamples * STEREO) >= ab->size) {
			/* split over two copies */
			//int chunk2 = ab->read_offset + (nsamples * STEREO) - ab->size;
			//int chunk1 = (nsamples * STEREO) - chunk2;
			//int chunk1 = nsamples / 2;
         //int chunk2 = nsamples / 2;
         printf("setting bread\n");
         int16_t* bread = pregen->buffers[i];
			int16_t* bwrite = curr->buffers[i];
         printf("memcpy 1\n");
         //memcpy (bwrite, bread, sizeof(int16_t) * nsamples * STEREO);
         bwrite = calloc(nsamples * STEREO, sizeof(int16_t));
         printf("memcpy 1 done\n");

			//bread += ab->read_offset;
			//memcpy(bwrite, bread, sizeof(int16_t) * chunk2);
			//bread = &ab->set.buffers[i];
			//bwrite += chunk2;
		} else {
         printf("setting bread 2\n");
			int16_t* bread = pregen->buffers[i];
			int16_t* bwrite = curr->buffers[i];
         printf("memcpy 2\n");
         bwrite = calloc(nsamples * STEREO, sizeof(int16_t));
         printf("memcpy 2 done\n");
			/*bread +=*/
		}
      printf("loop %d\n", i);
#endif
	}
   printf("returning\n");
   ab->read_offset += (nsamples * STEREO);
   if(ab->read_offset >= ab->size)
      ab->read_offset -= ab->size;

   ab->avail -= nsamples;
   num_calls--;
	return(curr);
}



