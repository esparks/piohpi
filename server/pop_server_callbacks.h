#ifndef POP_SERVER_CALLBACKS_H
#define POP_SERVER_CALLBACKS_H

#include "pop_server.h"


void handle_message(sequencer_data_t *seq,
                    unsigned char size,
                    unsigned char *message);

void send_plugin_events(sequencer_data_t *data);



#endif
