/* pop_server.h */
#ifndef POP_SERVER
#define POP_SERVER

#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include "../common/common_buffers.h"
#include "../common/pattern.h"


#define DEFAULT_PATTERN_NUM 255



typedef struct {
   void     *handle;
   void     *instance;
   void     *(*init)(void);
   int16_t  *(*gen_buffer)(void *instance, int samples);
   void     (*send_payload)(void *instance, event_t *event);
   void     (*set_master_volume)(void *instance, float);
} plugin_t;

typedef struct {
	buffer_type_t set;
	size_t	write_offset;
	size_t 	read_offset;
	size_t	size;
   size_t   avail;
} ring_buffer_t;

typedef struct {
	ring_buffer_t	audio_buffer;
   pthread_mutex_t *buffer_lock;
   int            last_read;
   plugin_t       *plugin;
   void           *mixer;
   time_t         last_time;
   time_t         last_time_fetch;
   unsigned char  playing;
   int            num_plugins;
   pattern_t**    pbank;
   pattern_t*     pattern;
   int            p_id; /* id of current pattern */
   unsigned char  changed; /* track updating */
} sequencer_data_t;



void do_quit();

#endif
