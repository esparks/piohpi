/* standard header for all plugin files */
#ifndef PLUGIN_HEADER_H
#define PLUGIN_HEADER_H
#include "../common/pattern.h"
#include <stdint.h>

void *init_plugin(); /*returns an instance struct */
int16_t* get_buffer(void* instance, int samples);
void receive_payload(void* instance, event_t* event);
void set_master_volume(void* instance, float vol);

#endif
