/* Simple pulse wave synthesizer */
#include "plugin_header.h"
#include "../common/pattern.h"
#include <stdint.h>
#include <stdio.h>
#include "note_frequencies.h"

typedef struct {
   float env;
   int pitch;
   int waveshape;
   int flip;
   float gain;
   int s_count;
   float lpf_left;
   float lpf_right;
   float lpf_beta;
   float filter_env;
   float pw_lfo;
   int   pw_lfo_edge;
} instance_t;


void
*init_plugin()
{
   instance_t *instance = malloc(sizeof(instance_t));
   instance->env = 0.0;
   instance->pitch = 0;
   instance->waveshape = 0; /* prcntg. of samples before flip */
   instance->flip = 0;
   instance->gain = 0.2;
   instance->s_count = 0;
   instance->lpf_left = 0.0;
   instance->lpf_right = 0.0;
   instance->lpf_beta = 0.5;
   instance->filter_env = 0.0;
   instance->pw_lfo = 0.0;
   instance->pw_lfo_edge = 0;
   printf("Creating pulse synth\n");
   return (void*)(instance);
}

int16_t
*get_buffer(void *instance, int samples)
{
   instance_t *ins = instance;

   /* calculate how many samples to go before flipping based on pitch.
      we use a lookup table of precalculate note frequencies for the
      given temperament */

   int period = (int)((float)44100 / EQUAL_TEMPERAMENT[ins->pitch]);
   int max = INT16_MAX / 2;
   int min = 0;
   float env_per_sample = 0.0001;
   float f_env_per_sample = 0.0002;
   int s;
   int16_t *new_buff = malloc(sizeof(int16_t) * samples * 2);
   int16_t *write_head = new_buff;
   int writeval;
   float raw_l;
   float raw_r;
   float filter_env = ins->filter_env * ins->lpf_beta;

   float pulse_width = 0.50 * ins->pw_lfo + 0.25;
   int low = (int16_t)((float)period * pulse_width);
   int high = (period * 2) - low;
   float pw_lfo_speed = 0.000005;

   if (filter_env < 0.01) filter_env = 0.01;

   for (s = 0; s < samples; s++) {
      if (ins->pw_lfo_edge > 0) {
         ins->pw_lfo += pw_lfo_speed;
         if (ins->pw_lfo >= 1) {
           ins->pw_lfo_edge = 0;
         }
      } else {
         ins->pw_lfo -= pw_lfo_speed;
         if (ins->pw_lfo <= 0) {
            ins->pw_lfo_edge = 1;
         }
      }

      if (ins->env > 0) {
         if (ins->flip == 0) {
            writeval = max;
            period = high;
         } else {
            writeval = min;
            period = low;
         }
         
         raw_l = (int16_t)((float)writeval * ins->gain * ins->env);
         raw_r = (int16_t)((float)writeval * ins->gain * ins->env);
         ins->lpf_left = ins->lpf_left - (filter_env * (ins->lpf_left - raw_l));
         ins->lpf_right= ins->lpf_right - (filter_env * (ins->lpf_right - raw_r));
         *write_head++ = ins->lpf_left;
         *write_head++ = ins->lpf_right;
         ins->env -= env_per_sample;
         ins->filter_env -= f_env_per_sample;
         ins->s_count++;

         /* todo 16-04-16: move this outside of the if..env>0 condition
            should always be cycling */
         if (ins->s_count >= period) {
            ins->flip++;
            ins->flip = ins->flip % 2;
            ins->s_count = 0;
         }

      } else {
         *write_head++ = 0;
         *write_head++ = 0;
      }
   } 
   return(new_buff);
}

void
receive_payload(void* instance, event_t *event)
{
   instance_t *ins = instance;
   if (event->trigger > 0) {
      ins->env = 5.0;
      ins->filter_env = 1.0;
   }
   ins->pitch = event->note_val;
}

void 
set_master_volume(void* instance, float vol)
{
   instance_t *ins = instance;
   ins->gain = vol;
}
