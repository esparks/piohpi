/* Example plugin */
#include <stdlib.h>
#include <stdio.h>
#include "plugin_header.h"
#include "../common/wave_loader.h"
#include "../common/pattern.h"

static WaveFile *kickdrum;
static WaveFile *bass_c;
static WaveFile *hat;
static WaveFile *clap;
static WaveFile *cowbell;
static WaveFile *open_hat;
static WaveFile *hitty;
static WaveFile *ride_cymbal;

typedef struct {   
   WaveFile *current;
   int16_t *read_head;
   int head_pos;
   int playing;
   float gain;
} instance_t;



void
set_master_volume(void* instance, float gain)
{
   instance_t* ins = (instance_t*) instance;
   if(gain < 0.0)
      gain = 0.0;

   ins->gain = gain;

}

void*
new_instance()
{
   /* assembles a struct of desired type, returning it to the seq. to managee
      note: seq doesn't ever need to know contents thus struct remains void* */
   
   /* call from init and pass back */
   instance_t* ins = malloc(sizeof(instance_t));
   
   return (void*)(ins);
}

void* 
init_plugin()
{
   instance_t *ins = new_instance(); 
   static char loaded = 0;
   
   if (!loaded) {
      printf("Plugin initialising...\n");
      kickdrum = load_wav("../samples/kick.wav");
      bass_c = load_wav("../samples/bass_c.wav");
      hat = load_wav("../samples/closed_hat.wav");
      clap = load_wav("../samples/clap.wav");
      cowbell = load_wav("../samples/808_cowbell.wav");
      open_hat = load_wav("../samples/909_openhat.wav");
      hitty = load_wav("../samples/hitty.wav");
      ride_cymbal = load_wav("../samples/ride.wav");
      loaded = 1;
   }

   ins->current = kickdrum;
   ins->read_head = ins->current->data;
   ins->head_pos = 0;
   ins->playing = 0;
   ins->gain = 0.8f;
   return (void*)(ins);
}

void 
receive_payload(void *instance, event_t* event)
{
  instance_t* ins = (instance_t*)instance;
 /*printf("recieved a payload with trigger no. %d\n", event->trigger); */   
 
   if (event->trigger > 0) {


     switch(event->note_val) {
      case 1:
         ins->current = kickdrum;
         break;
      case 2:
         ins->current = bass_c;
         break;
      case 3: 
         ins->current = hat;
         break;
      case 4:
         ins->current = clap;
         break;
      case 5:
         ins->current = cowbell;
         break;
      case 6:
         ins->current = open_hat;
         break;
      case 7:
         ins->current = hitty;
         break;
      case 8:
         ins->current = ride_cymbal;
         break;
      default:
         ins->current = clap;
     }

      ins->playing = 1;
      ins->head_pos = 0;
      ins->read_head = ins->current->data;

   }
}

int16_t
*get_buffer(void *instance, int samples)
{
   instance_t* ins = (instance_t*)instance;

   int16_t *new_buff = malloc(sizeof(short) * samples * 2);
   int16_t *write_head = new_buff;
   int i;
   for (i = 0; i < samples; i++) {
      if(ins->head_pos > ins->current->length || ins->playing == 0) {
         *write_head++ = 0;
         *write_head++ = 0;
         ins->playing = 0;
      } else {
         *write_head++ = (int16_t)((float)*ins->read_head++ * ins->gain);
         *write_head++ = (int16_t)((float)*ins->read_head++ * ins->gain);
         ins->head_pos += 2;
      }
   }
  return (new_buff);
}
